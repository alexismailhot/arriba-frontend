# Arriba Flights - Frontend

## What it is

- This project was started as the second iteration of a website for a low-cost airline ticket alert company I had created, called Arriba Flights.
- The first iteration of the site was done with Wordpress... I know, mehhh. The reason I used Wordpress first is because at the time I started working on this project (a couple years back), it was the tool I had the most experience with.
- Due to changes in my life, I have now put this project on hold. Therefore the code is really not production-ready, but it's still a cool little proof of concept using React!

## Technologies

- React
- Typescript
- I18Next (Translations)
- Sentry (Error logging) 
- Tailwind CSS
- React testing library & Jest (Unit tests)
