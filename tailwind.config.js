module.exports = {
  content: [
    "./src/**/*.tsx",
  ],
  theme: {
    extend: {
      colors: {
        blue: {
          'light': 'var(--blue-light)',
          'normal': 'var(--blue-normal)'
        }
      }
    },
  },
  plugins: [],
}
