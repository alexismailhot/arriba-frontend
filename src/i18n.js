import { initReactI18next } from "react-i18next"
import i18n from "i18next"
import LanguageDetector from 'i18next-browser-languagedetector'
import FrenchTranslations from "./app/assets/translations/french_translation.json"
import EnglishTranslations from "./app/assets/translations/english_translation.json"

i18n
    .use(initReactI18next)
    .use(LanguageDetector)
    .init({
        fallbackLng: "en",
        interpolation: {
            escapeValue: false
        },
        detection: {
            order: ['cookie', 'htmlTag', 'localStorage', 'sessionStorage', 'navigator', 'path', 'subdomain'],
            caches: ['cookie']
        },
        resources: {
            en: {
                translation: EnglishTranslations
            },
            fr: {
                translation: FrenchTranslations
            },
        },
        react: {
            useSuspense: false
        }
    })

export default i18n