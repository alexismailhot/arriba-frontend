import { BrowserRouter } from 'react-router-dom'
import { CookiesProvider } from 'react-cookie'
import { BrowserTracing } from "@sentry/tracing"
import * as Sentry from "@sentry/react"
import React from 'react'
import ReactDOM from 'react-dom'
import App from 'app/App'
import './index.css'
import './i18n.js'

Sentry.init({
    environment: process.env.REACT_APP_SENTRY_ENVIRONMENT,
    dsn: "https://3e84de1814fe4c03962b9826c8d1e266@o374509.ingest.sentry.io/6231078",
    integrations: [new BrowserTracing()],
    tracesSampleRate: 1
})

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <CookiesProvider>
                <App />
            </CookiesProvider>
        </BrowserRouter>
    </React.StrictMode>,
    
    document.getElementById('root')
)
