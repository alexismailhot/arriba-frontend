import LoginRequest from "app/infra/http/requests/LoginRequest"
import LoginResponse from "app/infra/http/responses/LoginResponse"

class LoginStub {
    private static readonly USERNAME: string = "a_username"
    private static readonly PASSWORD: string = "a_password"
    private static readonly TOKEN: string = "a_token"

    static givenALoginRequest(): LoginRequest {
        return {
            username: this.USERNAME,
            password: this.PASSWORD
        }
    }

    static givenALoginResponse(): LoginResponse {
        return {
            token: this.TOKEN
        }
    }
}

export default LoginStub