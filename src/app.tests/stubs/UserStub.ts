import ForgotPasswordRequest from "app/infra/http/requests/ForgotPasswordRequest"
import ProfileResponse from "app/infra/http/responses/ProfileResponse"

class UserStub {
    private static readonly EMAIL: string = "an_email"
    private static readonly FIRST_NAME: string = "a_first_name"

    static givenAProfileResponse(): ProfileResponse {
        return {
            email: this.EMAIL,
            firstName: this.FIRST_NAME
        }
    }

    static givenAForgotPasswordRequest(): ForgotPasswordRequest {
        return {
            email: this.EMAIL
        }
    }
}

export default UserStub