class CookieMock {
    static mockDocumentCookie(): void {
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: ''
        })
    }

    static resetDocumentCookieMock(): void {
        document.cookie = ""
    }
}

export default CookieMock