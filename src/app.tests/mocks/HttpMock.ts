import { when } from 'jest-when'
import Http from 'app/infra/http/Http'

class HttpMock {
    static mockPostRequest(apiRoute: string, httpStatus: number, response: {} = {}): void {
        const promise = new Promise<Partial<Response>>((resolve) => {
            resolve({
                status: httpStatus,
                json: jest.fn().mockReturnValue(response)
            })
        })

        when(jest.spyOn(Http, 'postRequest'))
            .calledWith(apiRoute, expect.anything())
            .mockResolvedValue(promise as Promise<Response>)
    }

    static mockAuthenticatedGetRequest(apiRoute: string, httpStatus: number, response: {} = {}): void {
        const promise = new Promise<Partial<Response>>((resolve) => {
            resolve({
                status: httpStatus,
                json: jest.fn().mockReturnValue(response)
            })
        })

        when(jest.spyOn(Http, 'authenticatedGetRequest'))
            .calledWith(apiRoute, expect.anything())
            .mockResolvedValue(promise as Promise<Response>)
    }
}

export default HttpMock