import { when } from 'jest-when'

class FetchMock {
    static mockRequest(apiRoute: string, httpStatus: number, response: {} = {}): void {
        const promise = new Promise<Partial<Response>>((resolve) => {
            resolve({
                status: httpStatus,
                json: jest.fn().mockReturnValue(response)
            })
        })

        when(jest.spyOn(global, 'fetch'))
            .calledWith(apiRoute, expect.anything())
            .mockResolvedValue(promise as Promise<Response>)
    }
}

export default FetchMock