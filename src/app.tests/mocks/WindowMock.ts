class WindowMock {
    static mockWindow(): void {
        const location: Location = window.location
        // @ts-ignore
        delete window.location
        window.location = {
            ...location,
            reload: jest.fn()
        }
    }
}

export default WindowMock