import { act } from "react-dom/test-utils"
import { renderHook } from "@testing-library/react-hooks"
import CookieMock from "app.tests/mocks/CookieMock"
import useTokenCookie from "app/hooks/cookies/useTokenCookie"
import Cookie from "app/hooks/cookies/Cookie"

describe('useTokenCookie hook', () => {
    const TOKEN: string = "a_token"

    CookieMock.mockDocumentCookie()

    afterEach(() => {
        CookieMock.resetDocumentCookieMock()
    })

    it('should return undefined token if cookie is not set', () => {
        const { result } = renderHook(() => useTokenCookie())

        expect(result.current.token).toBe(undefined)
    })

    it('should return the token if it is set in cookies', () => {
        document.cookie = Cookie.Token + '=' + TOKEN + '; Path=/'

        const { result } = renderHook(() => useTokenCookie())

        expect(result.current.token).toBe(TOKEN)
    })

    describe('setTokenCookie method', () => {
        it('should set the cookie', async () => {
            const { result, waitForNextUpdate } = renderHook(() => useTokenCookie())

            await act(async () => {
                result.current.setTokenCookie(TOKEN)
                await waitForNextUpdate()
            })

            expect(result.current.token).toBe(TOKEN)
        })
    })

    describe('removeTokenCookie method', () => {
        it('should set the cookie', async () => {
            const { result, waitForNextUpdate } = renderHook(() => useTokenCookie())

            await act(async () => {
                result.current.removeTokenCookie()
                await waitForNextUpdate()
            })

            expect(result.current.token).toBe("")
        })
    })
})