import { renderHook } from "@testing-library/react-hooks"
import useStringHelper from "app/hooks/useStringHelper"

describe('useStringHelper hook', () => {
    describe('isEmptyString method', () => {
        it('should return false if string is not empty', () => {
            const { result } = renderHook(() => useStringHelper())

            const returnValue: boolean = result.current.isEmptyString("Not empty")

            expect(returnValue).toBe(false)
        })

        it('should return true if string is empty', () => {
            const { result } = renderHook(() => useStringHelper())

            const returnValue: boolean = result.current.isEmptyString("")

            expect(returnValue).toBe(true)
        })

        it('should return true if string contains only whitespaces', () => {
            const { result } = renderHook(() => useStringHelper())

            const returnValue: boolean = result.current.isEmptyString("      ")

            expect(returnValue).toBe(true)
        })
    })
})