import { renderHook } from "@testing-library/react-hooks"
import * as useRedirection from "app/hooks/redirections/useRedirection"
import HttpMock from "app.tests/mocks/HttpMock"
import UserStub from "app.tests/stubs/UserStub"
import ProfileFactory from "app/domain/factories/ProfileFactory"
import Profile from "app/domain/Profile"
import useUserRequests from "app/hooks/API/useUserRequest"
import APIError from "app/infra/http/APIError"
import APIRoute from "app/infra/http/APIRoute"
import Http from "app/infra/http/Http"
import ProfileResponse from "app/infra/http/responses/ProfileResponse"
import StatusCode from "app/infra/http/StatusCode"
import ForgotPasswordRequest from "app/infra/http/requests/ForgotPasswordRequest"

describe('useUserRequest hook', () => {
    const PROFILE_RESPONSE: ProfileResponse = UserStub.givenAProfileResponse()
    const FORGOT_PASSWORD_REQUEST: ForgotPasswordRequest = UserStub.givenAForgotPasswordRequest()
    const TOKEN: string = "A token"

    const redirectToError = jest.fn()

    let profileResponse: ProfileResponse

    beforeEach(() => {
        profileResponse = { ...PROFILE_RESPONSE }

        jest.spyOn(useRedirection, "default").mockReturnValue({
            redirectToPrecedentPage: jest.fn(),
            redirectToLogin: jest.fn(),
            redirectToDashboard: jest.fn(),
            redirectToSubscribe: jest.fn(),
            redirectToForgotPassword: jest.fn(),
            redirectToError: redirectToError
        })  

        HttpMock.mockAuthenticatedGetRequest(APIRoute.profile(), StatusCode.Ok, profileResponse)
        HttpMock.mockPostRequest(APIRoute.forgotPassword(), StatusCode.Ok)
    })

    describe('getProfileApiRequest method', () => {
        it('should send a profile authenticated GET request', async () => {
            const { result } = renderHook(() => useUserRequests())

            await result.current.getProfileApiRequest(TOKEN)

            expect(Http.authenticatedGetRequest).toHaveBeenCalledTimes(1)
            expect(Http.authenticatedGetRequest).toHaveBeenCalledWith(APIRoute.profile(), TOKEN)
        })

        describe('given the profile request succeeds', () => {
            describe('given the ProfileResponse is valid', () => {
                it('should return an object containing a Profile and an empty APIError', async () => {
                    const expectedProfile: Profile = ProfileFactory.create(profileResponse)
                    const { result } = renderHook(() => useUserRequests())

                    const returnValue = await result.current.getProfileApiRequest(TOKEN)

                    expect(returnValue).toStrictEqual({ profile: expectedProfile, apiError: undefined })
                })
            })

            describe('given the ProfileResponse is invalid', () => {
                beforeEach(() => {
                    // @ts-ignore
                    profileResponse.email = 1
                })

                it('should call the redirectToError hook', async () => {
                    const { result } = renderHook(() => useUserRequests())

                    await result.current.getProfileApiRequest(TOKEN)

                    expect(redirectToError).toHaveBeenCalledTimes(1)
                })

                it('should return an empty object', async () => {
                    const { result } = renderHook(() => useUserRequests())

                    const returnValue = await result.current.getProfileApiRequest(TOKEN)

                    expect(returnValue).toStrictEqual({})
                })
            })
        })

        describe('given the profile request fails', () => {
            it('should return an object containing an empty Profile and an APIError', async () => {
                const errorCode: string = "An error code"
                const errorMessage: string = "An error message"
                HttpMock.mockAuthenticatedGetRequest(APIRoute.profile(), StatusCode.Forbidden, { code: errorCode, message: errorMessage })
                const expectedError: APIError = new APIError(StatusCode.Forbidden, errorCode, errorMessage)
                const { result } = renderHook(() => useUserRequests())

                const returnValue = await result.current.getProfileApiRequest(TOKEN)

                expect(returnValue).toStrictEqual({ profile: undefined, apiError: expectedError })
            })
        })
    })

    describe('forgotPasswordApiRequest method', () => {
        it('should send a forgot password POST request', async () => {
            const { result } = renderHook(() => useUserRequests())

            await result.current.forgotPasswordApiRequest(FORGOT_PASSWORD_REQUEST)

            expect(Http.postRequest).toHaveBeenCalledTimes(1)
            expect(Http.postRequest).toHaveBeenCalledWith(APIRoute.forgotPassword(), FORGOT_PASSWORD_REQUEST)
        })

        describe('given the forgot password request succeeds', () => {
            it('should return an empty object', async () => {
                const { result } = renderHook(() => useUserRequests())

                const returnValue = await result.current.forgotPasswordApiRequest(FORGOT_PASSWORD_REQUEST)

                expect(returnValue).toStrictEqual({})
            })
        })

        describe('given the forgot password request fails', () => {
            it('should return an object containing an APIError', async () => {
                const errorCode: string = "An error code"
                const errorMessage: string = "An error message"
                HttpMock.mockPostRequest(APIRoute.forgotPassword(), StatusCode.Forbidden, { code: errorCode, message: errorMessage })
                const expectedError: APIError = new APIError(StatusCode.Forbidden, errorCode, errorMessage)
                const { result } = renderHook(() => useUserRequests())

                const returnValue = await result.current.forgotPasswordApiRequest(FORGOT_PASSWORD_REQUEST)

                expect(returnValue).toStrictEqual({ apiError: expectedError })
            })
        })
    })
})