import { renderHook } from "@testing-library/react-hooks"
import * as useRedirection from "app/hooks/redirections/useRedirection"
import LoginStub from "app.tests/stubs/LoginStub"
import useAuthenticationRequests from "app/hooks/API/useAuthenticationRequests"
import APIError from "app/infra/http/APIError"
import APIRoute from "app/infra/http/APIRoute"
import LoginRequest from "app/infra/http/requests/LoginRequest"
import LoginResponse from "app/infra/http/responses/LoginResponse"
import StatusCode from "app/infra/http/StatusCode"
import HttpMock from "app.tests/mocks/HttpMock"
import Http from "app/infra/http/Http"

describe('useAuthenticationRequests hook', () => {
    const LOGIN_REQUEST: LoginRequest = LoginStub.givenALoginRequest()
    const LOGIN_RESPONSE: LoginResponse = LoginStub.givenALoginResponse()

    const redirectToError = jest.fn()

    let loginResponse: LoginResponse

    beforeEach(() => {
        loginResponse = { ...LOGIN_RESPONSE }

        jest.spyOn(useRedirection, "default").mockReturnValue({
            redirectToPrecedentPage: jest.fn(),
            redirectToLogin: jest.fn(),
            redirectToDashboard: jest.fn(),
            redirectToSubscribe: jest.fn(),
            redirectToForgotPassword: jest.fn(),
            redirectToError: redirectToError
        })  

        HttpMock.mockPostRequest(APIRoute.login(), StatusCode.Ok, loginResponse)
    })

    describe('loginApiRequest method', () => {
        it('should send a login POST request', async () => {
            const { result } = renderHook(() => useAuthenticationRequests())

            await result.current.loginApiRequest(LOGIN_REQUEST)

            expect(Http.postRequest).toHaveBeenCalledTimes(1)
            expect(Http.postRequest).toHaveBeenCalledWith(APIRoute.login(), LOGIN_REQUEST)
        })

        describe('given the login POST request succeeds', () => {
            describe('given the LoginResponse is valid', () => {
                it('should return an object containing the token and an undefined APIError', async () => {
                    const { result } = renderHook(() => useAuthenticationRequests())

                    const returnValue = await result.current.loginApiRequest(LOGIN_REQUEST)

                    expect(returnValue).toStrictEqual({ token: loginResponse.token, apiError: undefined })
                })
            })

            describe('given the LoginResponse is invalid', () => {
                beforeEach(() => {
                    //@ts-ignore
                    loginResponse.token = 1
                    HttpMock.mockPostRequest(APIRoute.login(), StatusCode.Ok, loginResponse)
                })

                it('should redirect to error page', async () => {
                    const { result } = renderHook(() => useAuthenticationRequests())

                    await result.current.loginApiRequest(LOGIN_REQUEST)

                    expect(redirectToError).toHaveBeenCalledTimes(1)
                })

                it('should return an empty object', async () => {
                    const { result } = renderHook(() => useAuthenticationRequests())

                    const returnValue = await result.current.loginApiRequest(LOGIN_REQUEST)

                    expect(returnValue).toStrictEqual({})
                })
            })
        })

        describe('given the login POST request fails', () => {
            it('should return an object containing an undefined token and an APIError', async () => {
                const errorCode: string = "An error code"
                const errorMessage: string = "An error message"
                HttpMock.mockPostRequest(APIRoute.login(), StatusCode.Forbidden, { code: errorCode, message: errorMessage })
                const expectedError: APIError = new APIError(StatusCode.Forbidden, errorCode, errorMessage)
                const { result } = renderHook(() => useAuthenticationRequests())

                const returnValue = await result.current.loginApiRequest(LOGIN_REQUEST)

                expect(returnValue).toStrictEqual({ token: undefined, apiError: expectedError })
            })
        })
    })
})