import { renderHook } from "@testing-library/react-hooks"
import useRedirection from "app/hooks/redirections/useRedirection"
import AppPath from "app/routes/AppPath"

const mockNavigate = jest.fn()
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom') as any,
    useNavigate: () => mockNavigate,
}))

describe('useRedirection hook', () => {
    describe('redirectToPrecedentPage method', () => {
        it('should call navigate with -1', () => {
            const { result } = renderHook(() => useRedirection())

            result.current.redirectToPrecedentPage()

            expect(mockNavigate).toHaveBeenCalledTimes(1)
            expect(mockNavigate).toHaveBeenCalledWith(-1)
        })
    })

    describe('redirectToLogin method', () => {
        it('should call navigate with the login path', () => {
            const { result } = renderHook(() => useRedirection())

            result.current.redirectToLogin()

            expect(mockNavigate).toHaveBeenCalledTimes(1)
            expect(mockNavigate).toHaveBeenCalledWith(AppPath.Login)
        })
    })

    describe('redirectToDashboard method', () => {
        it('should call navigate with the dashboard path', () => {
            const { result } = renderHook(() => useRedirection())

            result.current.redirectToDashboard()

            expect(mockNavigate).toHaveBeenCalledTimes(1)
            expect(mockNavigate).toHaveBeenCalledWith(AppPath.Dashboard)
        })
    })

    describe('redirectToSubscribe method', () => {
        it('should call navigate with the subscribe path', () => {
            const { result } = renderHook(() => useRedirection())

            result.current.redirectToSubscribe()

            expect(mockNavigate).toHaveBeenCalledTimes(1)
            expect(mockNavigate).toHaveBeenCalledWith(AppPath.Subscribe)
        })
    })

    describe('redirectToForgotPassword method', () => {
        it('should call navigate with the forgot password path', () => {
            const { result } = renderHook(() => useRedirection())

            result.current.redirectToForgotPassword()

            expect(mockNavigate).toHaveBeenCalledTimes(1)
            expect(mockNavigate).toHaveBeenCalledWith(AppPath.ForgotPassword)
        })
    })

    describe('redirectToError method', () => {
        it('should call navigate with the error path', () => {
            const { result } = renderHook(() => useRedirection())

            result.current.redirectToError()

            expect(mockNavigate).toHaveBeenCalledTimes(1)
            expect(mockNavigate).toHaveBeenCalledWith(AppPath.Error)
        })
    })
})