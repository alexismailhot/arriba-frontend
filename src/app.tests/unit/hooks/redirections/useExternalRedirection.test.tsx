import { renderHook } from "@testing-library/react-hooks"
import * as useLanguageHelper from "app/hooks/language/useLanguageHelper"
import WindowMock from "app.tests/mocks/WindowMock"
import Language from "app/hooks/language/Language"
import ExternalUrl from "app/hooks/redirections/ExternalUrl"
import useExternalRedirection from "app/hooks/redirections/useExternalRedirection"

describe('useExternalRedirection hook', () => {
    const WEBSITE_URL: string = "Website URL"
    process.env.REACT_APP_WEBSITE_URL = WEBSITE_URL

    const getCurrentLanguage = jest.fn()

    beforeAll(() => {
        WindowMock.mockWindow()
    })

    describe('redirectToHome method', () => {
        describe('given current language is FR', () => {
            it('should redirect to the french URL', () => {
                getCurrentLanguage.mockReturnValue(Language.FR)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToHome()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.HomeFr}`)
            })
        })

        describe('given current language is EN', () => {
            it('should redirect to the english URL', () => {
                getCurrentLanguage.mockReturnValue(Language.EN)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToHome()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.HomeEn}`)
            })
        })
    })

    describe('redirectToFAQ method', () => {
        describe('given current language is FR', () => {
            it('should redirect to the french URL', () => {
                getCurrentLanguage.mockReturnValue(Language.FR)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToFAQ()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.FaqFr}`)
            })
        })

        describe('given current language is EN', () => {
            it('should redirect to the english URL', () => {
                getCurrentLanguage.mockReturnValue(Language.EN)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToFAQ()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.FaqEn}`)
            })
        })
    })

    describe('redirectToAboutUs method', () => {
        describe('given current language is FR', () => {
            it('should redirect to the french URL', () => {
                getCurrentLanguage.mockReturnValue(Language.FR)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToAboutUs()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.AboutUsFr}`)
            })
        })

        describe('given current language is EN', () => {
            it('should redirect to the english URL', () => {
                getCurrentLanguage.mockReturnValue(Language.EN)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToAboutUs()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.AboutUsEn}`)
            })
        })
    })

    describe('redirectToTermsAndConditions method', () => {
        describe('given current language is FR', () => {
            it('should redirect to the french URL', () => {
                getCurrentLanguage.mockReturnValue(Language.FR)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToTermsAndConditions()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.TermsAndConditionsFr}`)
            })
        })

        describe('given current language is EN', () => {
            it('should redirect to the english URL', () => {
                getCurrentLanguage.mockReturnValue(Language.EN)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToTermsAndConditions()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.TermsAndConditionsEn}`)
            })
        })
    })

    describe('redirectToPrivacyPolicy method', () => {
        describe('given current language is FR', () => {
            it('should redirect to the french URL', () => {
                getCurrentLanguage.mockReturnValue(Language.FR)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToPrivacyPolicy()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.PrivacyPolicyFr}`)
            })
        })

        describe('given current language is EN', () => {
            it('should redirect to the english URL', () => {
                getCurrentLanguage.mockReturnValue(Language.EN)
                mockLanguageHelperHook()
                const { result } = renderHook(() => useExternalRedirection())

                result.current.redirectToPrivacyPolicy()

                expect(window.location.href).toBe(`${WEBSITE_URL}${ExternalUrl.PrivacyPolicyEn}`)
            })
        })
    })

    describe('redirectToInstagramProfile method', () => {
        it('should open the instagram profile in a new page', () => {
            window.open = jest.fn()
            mockLanguageHelperHook()
            const { result } = renderHook(() => useExternalRedirection())

            result.current.redirectToInstagramProfile()

            expect(window.open).toHaveBeenCalledTimes(1)
            expect(window.open).toHaveBeenCalledWith(ExternalUrl.InstagramProfile, '_blank')
        })
    })

    describe('redirectToFacebookProfile method', () => {
        it('should open the instagram profile in a new page', () => {
            window.open = jest.fn()
            mockLanguageHelperHook()
            const { result } = renderHook(() => useExternalRedirection())

            result.current.redirectToFacebookProfile()

            expect(window.open).toHaveBeenCalledTimes(1)
            expect(window.open).toHaveBeenCalledWith(ExternalUrl.FacebookProfile, '_blank')
        })
    })

    describe('redirectToTwitterProfile method', () => {
        it('should open the instagram profile in a new page', () => {
            window.open = jest.fn()
            mockLanguageHelperHook()
            const { result } = renderHook(() => useExternalRedirection())

            result.current.redirectToTwitterProfile()

            expect(window.open).toHaveBeenCalledTimes(1)
            expect(window.open).toHaveBeenCalledWith(ExternalUrl.TwitterProfile, '_blank')
        })
    })

    const mockLanguageHelperHook = (): void => {
        jest.spyOn(useLanguageHelper, "default").mockReturnValue({
            getCurrentLanguage,
            getOppositeLanguage: jest.fn(),
            changeLanguage: jest.fn()
        })  
    }
})