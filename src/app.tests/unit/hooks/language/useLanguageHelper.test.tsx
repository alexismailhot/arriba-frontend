import { renderHook } from "@testing-library/react-hooks"
import Language from "app/hooks/language/Language"
import useLanguageHelper from "app/hooks/language/useLanguageHelper"
import I18next from "i18next"

describe('useLanguageHelper hook', () => {
    describe('getCurrentLanguage method', () => {
        it('should return I18next current language', () => {
            I18next.language = Language.EN
            const { result } = renderHook(() => useLanguageHelper())

            const currentLanguage: string = result.current.getCurrentLanguage()

            expect(currentLanguage).toBe(Language.EN)
        })
    })

    describe('getOppositeLanguage method', () => {
        it('should return FR if I18next current language is EN', () => {
            I18next.language = Language.EN
            const { result } = renderHook(() => useLanguageHelper())

            const oppositeLanguage: string = result.current.getOppositeLanguage()

            expect(oppositeLanguage).toBe(Language.FR)
        })

        it('should return EN if I18next current language is FR', () => {
            I18next.language = Language.FR
            const { result } = renderHook(() => useLanguageHelper())

            const oppositeLanguage: string = result.current.getOppositeLanguage()

            expect(oppositeLanguage).toBe(Language.EN)
        })
    })
})