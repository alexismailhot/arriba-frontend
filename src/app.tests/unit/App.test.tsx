import { render, screen } from "@testing-library/react"
import App from "app/App"

const MOCKED_HEADER: string = 'Mocked header'
jest.mock('app/components/header/Header', () => {
    return {
        __esModule: true,
        default: () => (
            <p>{MOCKED_HEADER}</p>
        )
    }
})

const MOCKED_APP_CONTENT: string = 'Mocked app content'
jest.mock('app/routes/AppContent', () => {
    return {
        __esModule: true,
        default: () => (
            <p>{MOCKED_APP_CONTENT}</p>
        )
    }
})

const MOCKED_FOOTER: string = 'Mocked footer'
jest.mock('app/components/Footer', () => {
    return {
        __esModule: true,
        default: () => (
            <p>{MOCKED_FOOTER}</p>
        )
    }
})

describe('App component', () => {
    beforeEach(() => {
        render(
            <App />
        )
    })

    it('should display the header', () => {
        expect(screen.getByText(MOCKED_HEADER)).toBeInTheDocument()
    })

    it('should display the app content', () => {
        expect(screen.getByText(MOCKED_APP_CONTENT)).toBeInTheDocument()
    })

    it('should display the footer', () => {
        expect(screen.getByText(MOCKED_FOOTER)).toBeInTheDocument()
    })
})