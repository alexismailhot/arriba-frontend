import APIError from "app/infra/http/APIError"
import StatusCode from "app/infra/http/StatusCode"

describe('APIError class', () => {
    const STATUS: number = 200
    const CODE: string = "code"
    const MESSAGE: string = "message"

    describe('constructor method', () => {
        it('should create an APIError given all parameters are defined', () => {
            const result: APIError = new APIError(STATUS, CODE, MESSAGE)

            expect(result.status).toBe(STATUS)
            expect(result.code).toBe(CODE)
            expect(result.message).toBe(MESSAGE)
        })

        it('should create an APIError with empty code given code parameter is undefined', () => {
            const result: APIError = new APIError(STATUS, undefined, MESSAGE)

            expect(result.status).toBe(STATUS)
            expect(result.code).toBe("")
            expect(result.message).toBe(MESSAGE)
        })

        it('should create an APIError with empty message given message parameter is undefined', () => {
            const result: APIError = new APIError(STATUS, CODE, undefined)

            expect(result.status).toBe(STATUS)
            expect(result.code).toBe(CODE)
            expect(result.message).toBe("")
        })
    })

    describe('isUnauthorized method', () => {
        it('should return true if status is Forbidden or Unauthorized', () => {
            const forbiddenError: APIError = new APIError(StatusCode.Forbidden, CODE, MESSAGE)
            const unauthorizedError: APIError = new APIError(StatusCode.Unauthorized, CODE, MESSAGE)

            expect(forbiddenError.isUnauthorized()).toBe(true)
            expect(unauthorizedError.isUnauthorized()).toBe(true)
        })

        it('should return false if status is anything else', () => {
            const apiError: APIError = new APIError(404, CODE, MESSAGE)

            expect(apiError.isUnauthorized()).toBe(false)
        })
    })

    describe('isInvalidEmailCode method', () => {
        it('should return true if code is invalid email', () => {
            const apiError: APIError = new APIError(STATUS, "Some error code: invalid_email", MESSAGE)

            expect(apiError.isInvalidEmailCode()).toBe(true)
        })

        it('should return false if code is anything else', () => {
            const apiError: APIError = new APIError(STATUS, "Some error code: other_reason", MESSAGE)

            expect(apiError.isInvalidEmailCode()).toBe(false)
        })
    })

    describe('isInvalidPasswordCode method', () => {
        it('should return true if code is invalid password', () => {
            const apiError: APIError = new APIError(STATUS, "Some error code: incorrect_password", MESSAGE)

            expect(apiError.isInvalidPasswordCode()).toBe(true)
        })

        it('should return false if code is anything else', () => {
            const apiError: APIError = new APIError(STATUS, "Some error code: other_reason", MESSAGE)

            expect(apiError.isInvalidPasswordCode()).toBe(false)
        })
    })

    describe('isInvalidUsernameCode method', () => {
        it('should return true if code is invalid username', () => {
            const apiError: APIError = new APIError(STATUS, "Some error code: invalid_username", MESSAGE)

            expect(apiError.isInvalidUsernameCode()).toBe(true)
        })

        it('should return false if code is anything else', () => {
            const apiError: APIError = new APIError(STATUS, "Some error code: other_reason", MESSAGE)

            expect(apiError.isInvalidUsernameCode()).toBe(false)
        })
    })
})