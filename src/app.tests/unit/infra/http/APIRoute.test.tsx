import APIRoute from "app/infra/http/APIRoute"

describe('APIRoute class', () => {
    describe('login method', () => {
        it('should return the login API route', () => {
            expect(APIRoute.login()).toBe('/jwt-auth/v1/token')
        })
    })

    describe('profile method', () => {
        it('should return the profile API route', () => {
            expect(APIRoute.profile()).toBe('/users/profile')
        })
    })

    describe('forgotPassword method', () => {
        it('should return the forgot password API route', () => {
            expect(APIRoute.forgotPassword()).toBe('/users/forgotPassword')
        })
    })
})