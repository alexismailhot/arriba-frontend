import UserStub from "app.tests/stubs/UserStub"
import ProfileResponse from "app/infra/http/responses/ProfileResponse"
import ProfileResponseValidator from "app/infra/http/responses/validators/ProfileResponseValidator"

describe('ProfileResponseValidator class', () => {
    const PROFILE_RESPONSE: ProfileResponse = UserStub.givenAProfileResponse()

    describe('validate method', () => {
        it('should not throw if ProfileResponse is valid', () => {
            expect(() => ProfileResponseValidator.validate(PROFILE_RESPONSE)).not.toThrow()
        })

        it('should throw if email is not a string', () => {
            const invalidProfileResponse: any = { ...PROFILE_RESPONSE, email: 1 }

            expect(() => ProfileResponseValidator.validate(invalidProfileResponse)).toThrow("The ProfileResponse email field should be a string, received number")
        })

        it('should throw if first name is not a string', () => {
            const invalidProfileResponse: any = { ...PROFILE_RESPONSE, firstName: 1 }

            expect(() => ProfileResponseValidator.validate(invalidProfileResponse)).toThrow("The ProfileResponse firstName field should be a string, received number")
        })
    })
})