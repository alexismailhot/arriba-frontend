import LoginStub from "app.tests/stubs/LoginStub"
import LoginResponse from "app/infra/http/responses/LoginResponse"
import LoginResponseValidator from "app/infra/http/responses/validators/LoginResponseValidator"

describe('LoginResponseValidator class', () => {
    const LOGIN_RESPONSE: LoginResponse = LoginStub.givenALoginResponse()

    describe('validate method', () => {
        it('should not throw if LoginResponse is valid', () => {
            expect(() => LoginResponseValidator.validate(LOGIN_RESPONSE)).not.toThrow()
        })

        it('should throw if token is not a string', () => {
            const invalidLoginResponse: any = { ...LOGIN_RESPONSE, token: 1 }

            expect(() => LoginResponseValidator.validate(invalidLoginResponse)).toThrow("The LoginResponse token field should be a string, received number")
        })
    })
})