import FetchMock from "app.tests/mocks/FetchMock"
import Http from "app/infra/http/Http"

describe('Http class', () => {
    const API_URL: string = "api_url"
    const PATH: string = "a_path"
    const BODY: any = { key: "value" }
    const TOKEN: string = "a_token"

    beforeEach(() => {
        process.env.REACT_APP_API_URL = API_URL

        FetchMock.mockRequest(`${API_URL}${PATH}`, 200)
    })

    describe('postRequest method', () => {
        it('should send a POST fetch request', async () => {
            await Http.postRequest(PATH, BODY)

            expect(fetch).toHaveBeenCalledTimes(1)
            expect(fetch).toHaveBeenCalledWith(
                `${API_URL}${PATH}`,
                {
                    method: 'POST',
                    headers: { 
                        'Content-type': 'application/json'
                    },
                    body: JSON.stringify(BODY),
                    credentials: 'include',
                    mode: 'cors'
                }
            )
        })
    })

    describe('authenticatedPostRequest method', () => {
        it('should send an authenticated POST fetch request', async () => {
            await Http.authenticatedPostRequest(PATH, BODY, TOKEN)

            expect(fetch).toHaveBeenCalledTimes(1)
            expect(fetch).toHaveBeenCalledWith(
                `${API_URL}${PATH}`,
                {
                    method: 'POST',
                    headers: { 
                        'Content-type': 'application/json',
                        'Authorization': `Bearer ${TOKEN}`
                    },
                    body: JSON.stringify(BODY),
                    credentials: 'include',
                    mode: 'cors'
                }
            )
        })
    })

    describe('authenticatedGetRequest method', () => {
        it('should send an authenticated GET fetch request', async () => {
            await Http.authenticatedGetRequest(PATH, TOKEN)

            expect(fetch).toHaveBeenCalledTimes(1)
            expect(fetch).toHaveBeenCalledWith(
                `${API_URL}${PATH}`,
                {
                    method: 'GET',
                    headers: { 
                        'Authorization': `Bearer ${TOKEN}`
                    },
                    credentials: 'include',
                    mode: 'cors'
                }
            )
        })
    })
})