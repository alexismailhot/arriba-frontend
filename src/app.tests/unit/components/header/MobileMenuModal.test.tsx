import { render, screen } from "@testing-library/react"
import { BrowserRouter } from "react-router-dom"
import { I18nextProvider } from 'react-i18next'
import * as useRedirection from "app/hooks/redirections/useRedirection"
import * as useExternalRedirection from "app/hooks/redirections/useExternalRedirection"
import I18n from "i18n.js"
import MobileMenuModal from "app/components/header/MobileMenuModal"
import UserEvent from "@testing-library/user-event"

describe('MobileMenuModal component', () => {
    const CLOSE_MODAL_ICON: string = "×"
    const ARRIBA_LOGO_ALT_TEXT: string = "Back to home"
    const SUBSCRIBE_BUTTON: string = "Subscribe"
    const FAQ: string = "Faq"
    const ABOUT_US: string = "About us"
    const LOGIN: string = "Login"
    const MY_ACCOUNT: string = "My account"

    const closeMobileMenu = jest.fn()
    const redirectToHome = jest.fn()
    const redirectToFAQ = jest.fn()
    const redirectToAboutUs = jest.fn()
    const redirectToLogin = jest.fn()
    const redirectToDashboard = jest.fn()
    const redirectToSubscribe = jest.fn()

    let isAuthenticated: boolean

    beforeEach(() => {
        isAuthenticated = true
    })

    it('should should display an icon to close the modal', () => {
        renderComponent()

        expect(screen.getByText(CLOSE_MODAL_ICON)).toBeInTheDocument()
    })

    it('should call the closeMobileMenu props method when clicking on the close icon', () => {
        renderComponent()

        UserEvent.click(screen.getByText(CLOSE_MODAL_ICON))

        expect(closeMobileMenu).toHaveBeenCalledTimes(1)
    })

    it('should display the Arriba logo', () => {
        renderComponent()

        expect(screen.getByAltText(ARRIBA_LOGO_ALT_TEXT)).toBeInTheDocument()
    })

    it('should call the redirectToHome hook method when clicking on the logo', () => {
        renderComponent()

        UserEvent.click(screen.getByAltText(ARRIBA_LOGO_ALT_TEXT))

        expect(redirectToHome).toHaveBeenCalledTimes(1)
    })

    it('should display a subscribe button', () => {
        renderComponent()

        expect(screen.getByText(SUBSCRIBE_BUTTON)).toBeInTheDocument()
    })

    describe('when clicking on the subscribe button', () => {
        it('should call the closeMobileMenu props method', () => {
            renderComponent()

            UserEvent.click(screen.getByText(SUBSCRIBE_BUTTON))

            expect(closeMobileMenu).toHaveBeenCalledTimes(1)
        })

        it('should call the redirectToSubscribe hook method', () => {
            renderComponent()

            UserEvent.click(screen.getByText(SUBSCRIBE_BUTTON))

            expect(redirectToSubscribe).toHaveBeenCalledTimes(1)
        })
    })

    it('should display a FAQ option', () => {
        renderComponent()

        expect(screen.getByText(FAQ)).toBeInTheDocument()
    })

    it('should call the redirectToFAQ hook method when clicking on the FAQ option', () => {
        renderComponent()

        UserEvent.click(screen.getByText(FAQ))

        expect(redirectToFAQ).toHaveBeenCalledTimes(1)
    })

    it('should display an about us option', () => {
        renderComponent()

        expect(screen.getByText(ABOUT_US)).toBeInTheDocument()
    })

    it('should call the redirectToAboutUs hook method when clicking on the about us option', () => {
        renderComponent()

        UserEvent.click(screen.getByText(ABOUT_US))

        expect(redirectToAboutUs).toHaveBeenCalledTimes(1)
    })

    describe('given user is authenticated', () => {
        beforeEach(() => {
            isAuthenticated = true
        })

        it('should display a my account option', () => {
            renderComponent()

            expect(screen.getByText(MY_ACCOUNT)).toBeInTheDocument()
            expect(screen.queryByText(LOGIN)).not.toBeInTheDocument()
        })

        describe('when clicking on the my account option', () => {
            it('should call the closeMobileMenu props method', () => {
                renderComponent()

                UserEvent.click(screen.getByText(MY_ACCOUNT))
    
                expect(closeMobileMenu).toHaveBeenCalledTimes(1)
            })
    
            it('should call the redirectToDashboard hook method', () => {
                renderComponent()

                UserEvent.click(screen.getByText(MY_ACCOUNT))
    
                expect(redirectToDashboard).toHaveBeenCalledTimes(1)
            })
        })
    })

    describe('given user is not authenticated', () => {
        beforeEach(() => {
            isAuthenticated = false
        })

        it('should display a login option', () => {
            renderComponent()

            expect(screen.getByText(LOGIN)).toBeInTheDocument()
            expect(screen.queryByText(MY_ACCOUNT)).not.toBeInTheDocument()
        })

        describe('when clicking on the login option', () => {
            it('should call the closeMobileMenu props method', () => {
                renderComponent()

                UserEvent.click(screen.getByText(LOGIN))
    
                expect(closeMobileMenu).toHaveBeenCalledTimes(1)
            })
    
            it('should call the redirectToLogin hook method', () => {
                renderComponent()

                UserEvent.click(screen.getByText(LOGIN))
    
                expect(redirectToLogin).toHaveBeenCalledTimes(1)
            })
        })
    })

    const renderComponent = (): void => {
        jest.spyOn(useRedirection, "default").mockReturnValue({
            redirectToPrecedentPage: jest.fn(),
            redirectToLogin, 
            redirectToDashboard, 
            redirectToSubscribe, 
            redirectToForgotPassword: jest.fn(),
            redirectToError: jest.fn()
        })  
        
        jest.spyOn(useExternalRedirection, "default").mockReturnValue({
            redirectToHome,
            redirectToFAQ,
            redirectToAboutUs,
            redirectToTermsAndConditions: jest.fn(),
            redirectToPrivacyPolicy: jest.fn(),
            redirectToInstagramProfile: jest.fn(),
            redirectToFacebookProfile: jest.fn(),
            redirectToTwitterProfile: jest.fn()
        })  

        render(
            <BrowserRouter>
                <I18nextProvider i18n={I18n}>
                    <MobileMenuModal 
                        isAuthenticated={isAuthenticated}
                        closeMobileMenu={closeMobileMenu}
                    />
                </I18nextProvider>
            </BrowserRouter>
        )
    }
})