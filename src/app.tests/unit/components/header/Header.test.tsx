import { render, screen } from "@testing-library/react"
import { BrowserRouter } from "react-router-dom"
import { I18nextProvider } from 'react-i18next'
import { MobileMenuModalProps } from "app/components/header/MobileMenuModal"
import * as useExternalRedirection from "app/hooks/redirections/useExternalRedirection"
import * as useTokenCookie from "app/hooks/cookies/useTokenCookie"
import * as useRedirection from "app/hooks/redirections/useRedirection"
import Header from "app/components/header/Header"
import UserEvent from "@testing-library/user-event"
import I18n from "i18n.js"

const MOCKED_LANGUAGE_SWITCHER: string = 'Mocked language switcher'
jest.mock('app/components/common/LanguageSwitcher', () => {
    return {
        __esModule: true,
        default: () => (
            <p>{MOCKED_LANGUAGE_SWITCHER}</p>
        )
    }
})

const MOCKED_MOBILE_MENU_MODAL: string = 'Mocked mobile menu modal'
const CLOSE_MOBILE_MENU_MODAL: string = 'Close mobile menu'
jest.mock('app/components/header/MobileMenuModal', () => {
    return {
        __esModule: true,
        default: ({ closeMobileMenu }: MobileMenuModalProps) => (
            <div>
                <p>{MOCKED_MOBILE_MENU_MODAL}</p>
                <div data-testid={CLOSE_MOBILE_MENU_MODAL} onClick={closeMobileMenu}></div>
            </div>
        )
    }
})

describe('Header component', () => {
    const ARRIBA_LOGO_ALT_TEXT: string = "Back to home"
    const FAQ: string = "Faq"
    const ABOUT_US: string = "About us"
    const LOGIN: string = "Login"
    const MY_ACCOUNT: string = "My account"
    const TOKEN: string = "A token"
    const SUBSCRIBE_BUTTON: string = "Subscribe"
    const MOBILE_MENU_BURGER_ICON_TITLE: string = "Open menu"

    const redirectToHome = jest.fn()
    const redirectToFAQ = jest.fn()
    const redirectToAboutUs = jest.fn()
    const redirectToLogin = jest.fn()
    const redirectToDashboard = jest.fn()
    const redirectToSubscribe = jest.fn()

    let token: string | undefined

    beforeEach(() => {
        token = TOKEN
    })

    it('should not display the mobile menu modal by default', () => {
        renderComponent()

        expect(screen.queryByText(MOCKED_MOBILE_MENU_MODAL)).not.toBeInTheDocument()
    })

    it('should display the mobile menu burger', () => {
        renderComponent()

        expect(screen.getByTitle(MOBILE_MENU_BURGER_ICON_TITLE)).toBeInTheDocument()
    })

    it('should open the mobile menu modal when clicking on the burger', () => {
        renderComponent()

        UserEvent.click(screen.getByTitle(MOBILE_MENU_BURGER_ICON_TITLE))

        expect(screen.getByText(MOCKED_MOBILE_MENU_MODAL)).toBeInTheDocument()
    })

    it('should close the mobile menu modal when the close function is triggered from the modal', () => {
        renderComponent()
        UserEvent.click(screen.getByTitle(MOBILE_MENU_BURGER_ICON_TITLE))

        UserEvent.click(screen.getByTestId(CLOSE_MOBILE_MENU_MODAL))

        expect(screen.queryByText(MOCKED_MOBILE_MENU_MODAL)).not.toBeInTheDocument()
    })

    it('should dislay the Arriba logo', () => {
        renderComponent()

        expect(screen.getAllByAltText(ARRIBA_LOGO_ALT_TEXT)[0]).toBeInTheDocument()
        expect(screen.getAllByAltText(ARRIBA_LOGO_ALT_TEXT)[1]).toBeInTheDocument()
    })

    it('should call the redirectToHome hook method when clicking on the Arriba logo', () => {
        renderComponent()

        UserEvent.click(screen.getAllByAltText(ARRIBA_LOGO_ALT_TEXT)[0])

        expect(redirectToHome).toHaveBeenCalledTimes(1)
    })

    it('should display a FAQ option', () => {
        renderComponent()

        expect(screen.getByText(FAQ)).toBeInTheDocument()
    })

    it('should call the redirectToFAQ hook method when clicking on the FAQ option', () => {
        renderComponent()

        UserEvent.click(screen.getByText(FAQ))

        expect(redirectToFAQ).toHaveBeenCalledTimes(1)
    })

    it('should display an about us option', () => {
        renderComponent()

        expect(screen.getByText(ABOUT_US)).toBeInTheDocument()
    })

    it('should call the redirectToAboutUs hook method when clicking on the about us option', () => {
        renderComponent()

        UserEvent.click(screen.getByText(ABOUT_US))

        expect(redirectToAboutUs).toHaveBeenCalledTimes(1)
    })

    describe('given user is authenticated', () => {
        beforeEach(() => {
            token = TOKEN
        })

        it('should display a my account option', () => {
            renderComponent()

            expect(screen.getByText(MY_ACCOUNT)).toBeInTheDocument()
            expect(screen.queryByText(LOGIN)).not.toBeInTheDocument()
        })

        it('should call the redirectToDashboard hook method when clicking on the my account option', () => {
            renderComponent()

            UserEvent.click(screen.getByText(MY_ACCOUNT))

            expect(redirectToDashboard).toHaveBeenCalledTimes(1)
        })
    })

    describe('given user is not authenticated', () => {
        beforeEach(() => {
            token = undefined
        })

        it('should display a login option', () => {
            renderComponent()

            expect(screen.getByText(LOGIN)).toBeInTheDocument()
            expect(screen.queryByText(MY_ACCOUNT)).not.toBeInTheDocument()
        })

        it('should call the redirectToLogin hook method when clicking on the login option', () => {
            renderComponent()

            UserEvent.click(screen.getByText(LOGIN))

            expect(redirectToLogin).toHaveBeenCalledTimes(1)
        })
    })

    it('should display a subscribe button', () => {
        renderComponent()

        expect(screen.getByText(SUBSCRIBE_BUTTON)).toBeInTheDocument()
    })

    it('should call the redirectToSubscribe hook method when clicking on the button', () => {
        renderComponent()

        UserEvent.click(screen.getByText(SUBSCRIBE_BUTTON))

        expect(redirectToSubscribe).toHaveBeenCalledTimes(1)
    })

    it('should display the LanguageSwitcher component', () => {
        renderComponent()

        expect(screen.getByText(MOCKED_LANGUAGE_SWITCHER)).toBeInTheDocument()
    })

    const renderComponent = (): void => {
        jest.spyOn(useTokenCookie, "default").mockReturnValue({
            token, 
            tokenLoading: false, 
            setTokenCookie: jest.fn(), 
            removeTokenCookie: jest.fn()
        })  

        jest.spyOn(useRedirection, "default").mockReturnValue({
            redirectToPrecedentPage: jest.fn(),
            redirectToLogin, 
            redirectToDashboard, 
            redirectToSubscribe, 
            redirectToForgotPassword: jest.fn(),
            redirectToError: jest.fn()
        })  
        
        jest.spyOn(useExternalRedirection, "default").mockReturnValue({
            redirectToHome,
            redirectToFAQ,
            redirectToAboutUs,
            redirectToTermsAndConditions: jest.fn(),
            redirectToPrivacyPolicy: jest.fn(),
            redirectToInstagramProfile: jest.fn(),
            redirectToFacebookProfile: jest.fn(),
            redirectToTwitterProfile: jest.fn()
        })  

        render(
            <BrowserRouter>
                <I18nextProvider i18n={I18n}>
                    <Header />
                </I18nextProvider>
            </BrowserRouter>
        )
    }
})