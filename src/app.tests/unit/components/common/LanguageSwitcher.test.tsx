import { render, screen } from "@testing-library/react"
import * as useLanguageHelper from "app/hooks/language/useLanguageHelper"
import LanguageSwitcher from "app/components/common/LanguageSwitcher"
import Language from "app/hooks/language/Language"
import UserEvent from "@testing-library/user-event"

describe('LanguageSwitcher component', () => {
    const getOppositeLanguage = jest.fn()
    const changeLanguage = jest.fn()

    it('should display the opposite language', () => {
        getOppositeLanguage.mockReturnValue(Language.EN)

        renderComponent()

        expect(screen.getByText(Language.EN)).toBeInTheDocument()
    })

    it('should call the changeLanguage hook method when clicking on the opposite language', () => {
        getOppositeLanguage.mockReturnValue(Language.EN)
        renderComponent()

        UserEvent.click(screen.getByText(Language.EN))

        expect(changeLanguage).toHaveBeenCalledTimes(1)
    })

    const renderComponent = (): void => {
        jest.spyOn(useLanguageHelper, 'default').mockReturnValue({
            getCurrentLanguage: jest.fn(),
            getOppositeLanguage,
            changeLanguage
        })

        render(
            <LanguageSwitcher />
        )
    }
})