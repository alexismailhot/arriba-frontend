import { render, screen } from "@testing-library/react"
import { I18nextProvider } from 'react-i18next'
import ErrorModal from "app/components/common/modals/ErrorModal"
import UserEvent from "@testing-library/user-event"
import I18n from "i18n.js"

describe('ErrorModal component', () => {
    const TITLE: string = "Modal title"
    const MESSAGE: string = "Modal message"
    const BUTTON_TEXT: string = "Modal button text"
    const IF_PROBLEM_PERSISTS: string = "If the problem persists, you can"
    const CONTACT_US_HERE: string = "contact us here."

    const onClose = jest.fn()

    let showContactUsText: boolean | undefined

    beforeEach(() => {
        showContactUsText = true
    })

    it('should display the title', () => {
        renderComponent()

        expect(screen.getByText(TITLE)).toBeInTheDocument()
    })

    it('should display the message', () => {
        renderComponent()

        expect(screen.getByText(MESSAGE)).toBeInTheDocument()
    })

    it('should display the button', () => {
        renderComponent()

        expect(screen.getByText(BUTTON_TEXT)).toBeInTheDocument()
    })

    describe('given the showContactUsText props is undefined', () => {
        beforeEach(() => {
            showContactUsText = undefined
        })

        it('should not show the contact us text', () => {
            renderComponent()

            expect(screen.queryByText(IF_PROBLEM_PERSISTS)).not.toBeInTheDocument()
            expect(screen.queryByText(CONTACT_US_HERE)).not.toBeInTheDocument()
        })
    })

    describe('given the showContactUsText props is false', () => {
        beforeEach(() => {
            showContactUsText = false
        })

        it('should not show the contact us text', () => {
            renderComponent()

            expect(screen.queryByText(IF_PROBLEM_PERSISTS)).not.toBeInTheDocument()
            expect(screen.queryByText(CONTACT_US_HERE)).not.toBeInTheDocument()
        })
    })

    describe('given the showContactUsText props is true', () => {
        beforeEach(() => {
            showContactUsText = true
        })

        it('should show the contact us text', () => {
            renderComponent()

            expect(screen.getByText(IF_PROBLEM_PERSISTS)).toBeInTheDocument()
            expect(screen.getByText(CONTACT_US_HERE)).toBeInTheDocument()
        })
    })

    it('should call the onClose props method when clicking on the button', () => {
        renderComponent()

        UserEvent.click(screen.getByText(BUTTON_TEXT))

        expect(onClose).toHaveBeenCalledTimes(1)
    })

    const renderComponent = (): void => {
        render(
            <I18nextProvider i18n={I18n}>
                <ErrorModal 
                    title={TITLE}
                    message={MESSAGE}
                    buttonText={BUTTON_TEXT}
                    onClose={onClose}
                    showContactUsText={showContactUsText}
                />
            </I18nextProvider>
        )
    }
})