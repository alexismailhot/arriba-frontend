import { render, screen } from "@testing-library/react"
import Modal from "app/components/common/modals/Modal"

describe('Modal component', () => {
    const CHILDREN_CONTENT: string = "A children"
    const CHILDREN: JSX.Element = <div>{CHILDREN_CONTENT}</div>

    beforeEach(() => {
        render(
            <Modal>
                {CHILDREN}
            </Modal>
        )
    })

    it('should render the children', () => {
        expect(screen.getByText(CHILDREN_CONTENT)).toBeInTheDocument()
    })
})