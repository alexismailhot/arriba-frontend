import { render, screen } from "@testing-library/react"
import InputErrorMessage from "app/components/common/inputs/InputErrorMessage"

describe('InputErrorMessage component', () => {
    const TEXT: string = "An error message"

    beforeEach(() => {
        render(
            <InputErrorMessage text={TEXT} />
        )
    })

    it('should display the error message', () => {
        expect(screen.getByText(TEXT)).toBeInTheDocument()
    })
})