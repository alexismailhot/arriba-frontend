import { render, screen } from "@testing-library/react"
import InputWrapper from "app/components/common/inputs/InputWrapper"

describe('InputWrapper component', () => {
    const CHILDREN_CONTENT: string = "A children"
    const CHILDREN: JSX.Element = <div>{CHILDREN_CONTENT}</div>

    beforeEach(() => {
        render(
            <InputWrapper>
                {CHILDREN}
            </InputWrapper>
        )
    })

    it('should render the children', () => {
        expect(screen.getByText(CHILDREN_CONTENT)).toBeInTheDocument()
    })
})