import { render, screen } from "@testing-library/react"
import Button from "app/components/common/Button"
import UserEvent from "@testing-library/user-event"

const MOCKED_LOAD_ANIMATION: string = 'Mocked load animation'
jest.mock('app/components/common/LoadAnimation', () => {
    return {
        __esModule: true,
        default: () => (
            <p>{MOCKED_LOAD_ANIMATION}</p>
        )
    }
})

describe('Button component', () => {
    const BUTTON_CONTENT: string = "Button content"

    const onClick = jest.fn()

    let disabled: boolean | undefined

    beforeEach(() => {
        disabled = undefined
    })

    it('should render a button', () => {
        renderComponent()

        expect(screen.getByText(BUTTON_CONTENT).closest('button')).toHaveAttribute("type", "button")
    })

    describe('given the disabled props is undefined', () => {
        beforeEach(() => {
            disabled = undefined
        })

        it('should render an enabled button', () => {
            renderComponent()
    
            expect(screen.getByText(BUTTON_CONTENT).closest('button')).not.toBeDisabled()
        })

        it('should not render the load animation', () => {
            renderComponent()

            expect(screen.queryByText(MOCKED_LOAD_ANIMATION)).not.toBeInTheDocument()
        })
    })

    describe('given the disabled props is false', () => {
        beforeEach(() => {
            disabled = false
        })

        it('should render an enabled button', () => {
            renderComponent()
    
            expect(screen.getByText(BUTTON_CONTENT).closest('button')).not.toBeDisabled()
        })

        it('should not render the load animation', () => {
            renderComponent()

            expect(screen.queryByText(MOCKED_LOAD_ANIMATION)).not.toBeInTheDocument()
        })
    })

    describe('given the disabled props is true', () => {
        beforeEach(() => {
            disabled = true
        })

        it('should render a disabled button', () => {
            renderComponent()
    
            expect(screen.getByText(BUTTON_CONTENT).closest('button')).toBeDisabled()
        })

        it('should render the load animation', () => {
            renderComponent()

            expect(screen.getByText(MOCKED_LOAD_ANIMATION)).toBeInTheDocument()
        })
    })

    it('should call the onClick props method when the button is clicked', () => {
        renderComponent()

        UserEvent.click(screen.getByText(BUTTON_CONTENT))

        expect(onClick).toHaveBeenCalledTimes(1)
    })

    const renderComponent = () => {
        render(
            <Button 
                content={BUTTON_CONTENT} 
                purpose={"secondary"} 
                disabled={disabled}
                onClick={onClick}               
            />
        )
    }
})