import { render, screen } from "@testing-library/react"
import RectangleImage from "app/components/common/images/RectangleImage"
import UserEvent from "@testing-library/user-event"

describe('RectangleImage component', () => {
    const ALT_TEXT: string = 'Image alt text'

    const onClick = jest.fn()

    beforeEach(() => {
        render(
            <RectangleImage
                source={'test/source'}
                alt={ALT_TEXT}
                width='20px'
                onClick={onClick}
            />
        )
    })

    it('should display the image', () => {
        expect(screen.getByAltText(ALT_TEXT)).toBeInTheDocument()
    })

    it('should call the onClick props method when the image is clicked', () => {
        UserEvent.click(screen.getByAltText(ALT_TEXT))

        expect(onClick).toHaveBeenCalledTimes(1)
    })
})