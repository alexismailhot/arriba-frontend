import { render, screen } from "@testing-library/react"
import PageWrap from "app/components/common/PageWrap"

describe('PageWrap component', () => {
    const CHILDREN: string = 'Some children content'

    beforeEach(() => {
        render(
            <PageWrap>
                <div>{CHILDREN}</div>
            </PageWrap>
        )
    })

    it('should display children', () => {
        expect(screen.getByText(CHILDREN)).toBeInTheDocument()
    })
})