import { render, screen } from "@testing-library/react"
import CenteredContent from "app/components/common/CenteredContent"

describe('CenteredContent component', () => {
    const CHILDREN: string = 'Some children content'

    beforeEach(() => {
        render(
            <CenteredContent>
                <div>{CHILDREN}</div>
            </CenteredContent>
        )
    })

    it('should display children', () => {
        expect(screen.getByText(CHILDREN)).toBeInTheDocument()
    })
})