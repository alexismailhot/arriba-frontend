import { render, screen } from "@testing-library/react"
import MailToLink from "app/components/common/links/MailToLink"

describe('MailToLink component', () => {
    const TEXT: string = "Some text"
    const EMAIL: string = "a_email@gmail.ca"
    const SUBJECT: string = "A subject"
    const BODY: string = "A body"

    let subject: string | undefined
    let body: string | undefined

    beforeEach(() => {
        subject = SUBJECT
        body = BODY
    })

    it('should display a link with href containing mailto, subject and body', () => {
        renderComponent()

        expect(screen.getByText(TEXT).closest('a')).toHaveAttribute('href', `mailto:${EMAIL}?subject=${SUBJECT}&body=${BODY}`)
    })

    describe('given subject is undefined', () => {
        beforeEach(() => {
            subject = undefined
        })

        it('should display a link with empty href subject', () => {
            renderComponent()

            expect(screen.getByText(TEXT).closest('a')).toHaveAttribute('href', `mailto:${EMAIL}?subject=&body=${BODY}`)
        })
    })

    describe('given body is undefined', () => {
        beforeEach(() => {
            body = undefined    
        })

        it('should display a link with empty href body', () => {
            renderComponent()
            
            expect(screen.getByText(TEXT).closest('a')).toHaveAttribute('href', `mailto:${EMAIL}?subject=${SUBJECT}&body=`)
        })
    })

    const renderComponent = (): void => {
        render(
            <MailToLink 
                linkText={TEXT} 
                email={EMAIL} 
                subject={subject} 
                body={body} 
            />
        )
    }
})