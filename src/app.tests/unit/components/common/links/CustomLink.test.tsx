import { render, screen } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import CustomLink from "app/components/common/links/CustomLink"

describe('CustomLink component', () => {
    const TEXT: string = "Some text"
    
    const onClick = jest.fn()

    let disabled: boolean | undefined
    
    beforeEach(() => {
        disabled = false
    })

    it('should display the link text received in props', () => {
        renderComponent()

        expect(screen.getByText(TEXT)).toBeInTheDocument()
    })

    describe('given disabled props is undefined', () => {
        beforeEach(() => {
            disabled = undefined
        })

        it('should call the onCLick props method when clicking on the text', () => {
            renderComponent()

            userEvent.click(screen.getByText(TEXT))

            expect(onClick).toHaveBeenCalledTimes(1)
        })
    })

    describe('given disabled props is false', () => {
        beforeEach(() => {
            disabled = false
        })

        it('should call the onCLick props method when clicking on the text', () => {
            renderComponent()

            userEvent.click(screen.getByText(TEXT))

            expect(onClick).toHaveBeenCalledTimes(1)
        })
    })

    describe('given disabled props is true', () => {
        beforeEach(() => {
            disabled = true
        })

        it('should not call the onCLick props method when clicking on the text', () => {
            renderComponent()

            userEvent.click(screen.getByText(TEXT))

            expect(onClick).not.toHaveBeenCalled()
        })
    })

    const renderComponent = (): void => {
        render(
            <CustomLink 
                text={TEXT}
                onClick={onClick}
                disabled={disabled}
            />
        )
    }
})