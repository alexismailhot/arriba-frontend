import { render, screen } from "@testing-library/react"
import { I18nextProvider } from 'react-i18next'
import * as useExternalRedirection from "app/hooks/redirections/useExternalRedirection"
import Footer from "app/components/Footer"
import I18n from "i18n.js"
import UserEvent from "@testing-library/user-event"

const MOCKED_LANGUAGE_SWITCHER: string = 'Mocked language switcher'
jest.mock('app/components/common/LanguageSwitcher', () => {
    return {
        __esModule: true,
        default: () => (
            <p>{MOCKED_LANGUAGE_SWITCHER}</p>
        )
    }
})

describe('Footer component', () => {
    const TERMS_AND_CONDITIONS_LINK: string = "Terms and conditions"
    const PRIVACY_POLICY_LINK: string = "Privacy policy"
    const INSTAGRAM_LOGO_ALT_TEXT: string = "Follow us on Instagram"
    const FACEBOOK_LOGO_ALT_TEXT: string = "Follow us on Facebook"
    const TWITTER_LOGO_ALT_TEXT: string = "Follow us on Twitter"

    const redirectToTermsAndConditions = jest.fn()
    const redirectToPrivacyPolicy = jest.fn()
    const redirectToInstagramProfile = jest.fn()
    const redirectToFacebookProfile = jest.fn()
    const redirectToTwitterProfile = jest.fn()

    beforeEach(() => {
        jest.spyOn(useExternalRedirection, "default").mockReturnValue({
            redirectToHome: jest.fn(),
            redirectToFAQ: jest.fn(),
            redirectToAboutUs: jest.fn(),
            redirectToTermsAndConditions,
            redirectToPrivacyPolicy,
            redirectToInstagramProfile,
            redirectToFacebookProfile,
            redirectToTwitterProfile
        })  

        render(
            <I18nextProvider i18n={I18n}>
                <Footer />
            </I18nextProvider>
        )
    })

    it('should display a terms and conditions link', () => {
        expect(screen.getByText(TERMS_AND_CONDITIONS_LINK)).toBeInTheDocument()
    })

    it('should call the redirectToTermsAndConditions hook method when clicking on the link', () => {
        UserEvent.click(screen.getByText(TERMS_AND_CONDITIONS_LINK))

        expect(redirectToTermsAndConditions).toHaveBeenCalledTimes(1)
    })

    it('should display a privacy policy link', () => {
        expect(screen.getByText(PRIVACY_POLICY_LINK)).toBeInTheDocument()
    })

    it('should call the redirectToPrivacyPolicy hook method when clicking on the link', () => {
        UserEvent.click(screen.getByText(PRIVACY_POLICY_LINK))

        expect(redirectToPrivacyPolicy).toHaveBeenCalledTimes(1)
    })

    it('should display the language switcher', () => {
        expect(screen.getByText(MOCKED_LANGUAGE_SWITCHER)).toBeInTheDocument()
    })

    it('should display a follow us text', () => {
        expect(screen.getByText("Follow us")).toBeInTheDocument()
    })

    it('should display an Instagram logo', () => {
        expect(screen.getByAltText(INSTAGRAM_LOGO_ALT_TEXT)).toBeInTheDocument()
    })

    it('should call the redirectToInstagramProfile hook method when clicking on the logo', () => {
        UserEvent.click(screen.getByAltText(INSTAGRAM_LOGO_ALT_TEXT))

        expect(redirectToInstagramProfile).toHaveBeenCalledTimes(1)
    })

    it('should display a Facebook logo', () => {
        expect(screen.getByAltText(FACEBOOK_LOGO_ALT_TEXT)).toBeInTheDocument()
    })

    it('should call the redirectToFacebookProfile hook method when clicking on the logo', () => {
        UserEvent.click(screen.getByAltText(FACEBOOK_LOGO_ALT_TEXT))

        expect(redirectToFacebookProfile).toHaveBeenCalledTimes(1)
    })

    it('should display a Twitter logo', () => {
        expect(screen.getByAltText(TWITTER_LOGO_ALT_TEXT)).toBeInTheDocument()
    })

    it('should call the redirectToTwitterProfile hook method when clicking on the logo', () => {
        UserEvent.click(screen.getByAltText(TWITTER_LOGO_ALT_TEXT))

        expect(redirectToTwitterProfile).toHaveBeenCalledTimes(1)
    })
})