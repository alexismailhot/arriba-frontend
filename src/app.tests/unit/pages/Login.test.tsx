import { render, screen } from "@testing-library/react"
import { BrowserRouter } from "react-router-dom"
import { act } from "react-dom/test-utils"
import { I18nextProvider } from 'react-i18next'
import * as useTokenCookie from "app/hooks/cookies/useTokenCookie"
import * as useRedirection from "app/hooks/redirections/useRedirection"
import * as useAuthenticationRequests from "app/hooks/API/useAuthenticationRequests"
import UserEvent from "@testing-library/user-event"
import Login from "app/pages/Login"
import LoginRequest from "app/infra/http/requests/LoginRequest"
import I18n from "i18n.js"
import APIError from "app/infra/http/APIError"

describe('Login component', () => {
    const TITLE: string = "Welcome back, globetrotter!"
    const LOGIN_BUTTON: string = "Login"
    const EMAIL_PLACEHOLDER: string = "Email"
    const PASSWORD_PLACEHOLDER: string = "Password"
    const TOKEN: string = "a_token"
    const EMAIL: string = "a_email"
    const PASSWORD: string = "a_password"
    const SUBSCRIBE_LINK: string = "Subscribe"
    const TROUBLE_LOGGING_IN_LINK: string = "Trouble logging in?"
    const EMAIL_ERROR_MESSAGE: string = "Email must not be empty."
    const PASSWORD_ERROR_MESSAGE: string = "Password must not be empty."
    const LOGIN_FAILED_MODAL_TITLE: string = "Login failed"
    const LOGIN_FAILED_MODAL_MESSAGE: string = "The email or password you entered seems invalid."
    const SOMETHING_WENT_WRONG_MODAL_TITLE: string = "Something went wrong"
    const SOMETHING_WENT_WRONG_MODAL_MESSAGE: string = "We're sorry, but there seems to be a problem on our end. Please try again, and if the problem persists, contact us."
    const CLOSE_BUTTON: string = "Close"
    const INVALID_EMAIL_ERROR_CODE: string = "invalid_email"

    const redirectToDashboard = jest.fn()
    const loginApiRequest = jest.fn()
    const setTokenCookie = jest.fn()
    const redirectToSubscribe = jest.fn()
    const redirectToForgotPassword = jest.fn()

    let token: string | undefined

    beforeEach(() => {
        token = undefined

        jest.spyOn(useRedirection, "default").mockReturnValue({
            redirectToPrecedentPage: jest.fn(),
            redirectToLogin: jest.fn(),
            redirectToDashboard,
            redirectToSubscribe,
            redirectToForgotPassword,
            redirectToError: jest.fn()
        })  
    })

    it('should show a welcome message', () => {
        renderComponent()

        expect(screen.getByText(TITLE)).toBeInTheDocument()
    })

    it('should show login input fields', () => {
        renderComponent()

        expect(screen.getByPlaceholderText(EMAIL_PLACEHOLDER).closest("input")).toHaveAttribute("type", "text")
        expect(screen.getByPlaceholderText(PASSWORD_PLACEHOLDER).closest("input")).toHaveAttribute("type", "password")
    })

    it('should show a login button', () => {
        renderComponent()

        expect(screen.getByText(LOGIN_BUTTON).closest("button")).toHaveAttribute("type", "button")
    })

    it('should show text to subscribe', () => {
        renderComponent()

        expect(screen.getByText("Don't have an account?")).toBeInTheDocument()
        expect(screen.getByText(SUBSCRIBE_LINK)).toBeInTheDocument()
    })

    it('should call the redirectToSubscribe hook method when clicking on the subscribe link', () => {
        renderComponent()

        UserEvent.click(screen.getByText(SUBSCRIBE_LINK))

        expect(redirectToSubscribe).toHaveBeenCalledTimes(1)
    })

    it('should show text to help logging in', () => {
        renderComponent()

        expect(screen.getByText(TROUBLE_LOGGING_IN_LINK)).toBeInTheDocument()
    })

    it('should call the redirectToForgotPassword hook method when clicking on the trouble logging in link', () => {
        renderComponent()

        UserEvent.click(screen.getByText(TROUBLE_LOGGING_IN_LINK))

        expect(redirectToForgotPassword).toHaveBeenCalledTimes(1)
    })

    it('should not show input error messages by default', () => {
        renderComponent()

        expect(screen.queryByText(EMAIL_ERROR_MESSAGE)).not.toBeInTheDocument()
        expect(screen.queryByText(PASSWORD_ERROR_MESSAGE)).not.toBeInTheDocument()
    })

    it('should not show modals by default', () => {
        renderComponent()

        expect(screen.queryByText(LOGIN_FAILED_MODAL_TITLE)).not.toBeInTheDocument()
        expect(screen.queryByText(LOGIN_FAILED_MODAL_MESSAGE)).not.toBeInTheDocument()
        expect(screen.queryByText(SOMETHING_WENT_WRONG_MODAL_TITLE)).not.toBeInTheDocument()
        expect(screen.queryByText(SOMETHING_WENT_WRONG_MODAL_MESSAGE)).not.toBeInTheDocument()
    })
    
    it('should redirect to dashboard page if token from cookies is set', () => {
        token = TOKEN

        renderComponent()
        
        expect(redirectToDashboard).toHaveBeenCalledTimes(1)
    })

    it('should not redirect to dashboard if token from cookies is undefined', () => {
        renderComponent()
        
        expect(redirectToDashboard).toHaveBeenCalledTimes(0)
    })

    describe('when clicking on the sign in button', () => {
        describe('given the user did not fill the login input fields', () => {
            beforeEach(async () => {
                renderComponent()
                await act(async () => {
                    UserEvent.click(screen.getByText(LOGIN_BUTTON))
                })
            })

            it('should show login fields error messages', () => {
                expect(screen.getByText(EMAIL_ERROR_MESSAGE)).toBeInTheDocument()
                expect(screen.getByText(PASSWORD_ERROR_MESSAGE)).toBeInTheDocument()
            })

            it('should hide login fields error messages when user starts typing', () => {
                UserEvent.type(screen.getByPlaceholderText(EMAIL_PLACEHOLDER), EMAIL)
                UserEvent.type(screen.getByPlaceholderText(PASSWORD_PLACEHOLDER), PASSWORD)

                expect(screen.queryByText(EMAIL_ERROR_MESSAGE)).not.toBeInTheDocument()
                expect(screen.queryByText(PASSWORD_ERROR_MESSAGE)).not.toBeInTheDocument()
            })
        })

        describe('given the user filled the login input fields', () => {
            it('should call the loginApiRequest hook method', async () => {
                loginApiRequest.mockReturnValue({ 
                    token: TOKEN,
                    apiError: undefined 
                })
                const expectedLoginRequest: LoginRequest = {
                    username: EMAIL,
                    password: PASSWORD
                }
                renderComponent()
                
                UserEvent.type(screen.getByPlaceholderText(EMAIL_PLACEHOLDER), EMAIL)
                UserEvent.type(screen.getByPlaceholderText(PASSWORD_PLACEHOLDER), PASSWORD)
                await act(async () => {
                    UserEvent.click(screen.getByText(LOGIN_BUTTON))
                })
                
                expect(loginApiRequest).toHaveBeenCalledTimes(1)
                expect(loginApiRequest).toHaveBeenCalledWith(expectedLoginRequest)
            })
    
            describe('given the loginApiRequest hook method returns a token', () => {
                beforeEach(async () => {
                    loginApiRequest.mockReturnValue({ 
                        token: TOKEN,
                        apiError: undefined 
                    })
                    renderComponent()
                    
                    UserEvent.type(screen.getByPlaceholderText(EMAIL_PLACEHOLDER), EMAIL)
                    UserEvent.type(screen.getByPlaceholderText(PASSWORD_PLACEHOLDER), PASSWORD)
                    await act(async () => {
                        UserEvent.click(screen.getByText(LOGIN_BUTTON))
                    })
                })
    
                it('should call the setTokenCookie hook method', () => {
                    expect(setTokenCookie).toHaveBeenCalledTimes(1) 
                    expect(setTokenCookie).toHaveBeenCalledWith(TOKEN) 
                })
    
                it('should redirect to dashboard', () => {
                    expect(redirectToDashboard).toHaveBeenCalledTimes(1)
                })
            })
    
            describe('given the loginApiRequest hook method returns an APIError', () => {
                describe('given the error occurred because of wrong credentials', () => {
                    beforeEach(async () => {
                        const apiError: APIError = new APIError(404, INVALID_EMAIL_ERROR_CODE, "Some error message")
                        loginApiRequest.mockReturnValue({ 
                            token: undefined,
                            apiError 
                        })
                        renderComponent()
                        
                        UserEvent.type(screen.getByPlaceholderText(EMAIL_PLACEHOLDER), EMAIL)
                        UserEvent.type(screen.getByPlaceholderText(PASSWORD_PLACEHOLDER), PASSWORD)
                        await act(async () => {
                            UserEvent.click(screen.getByText(LOGIN_BUTTON))
                        })
                    })

                    it('should show the login failed modal', () => {
                        expect(screen.getByText(LOGIN_FAILED_MODAL_TITLE)).toBeInTheDocument()
                        expect(screen.getByText(LOGIN_FAILED_MODAL_MESSAGE)).toBeInTheDocument()
                    })

                    it('should close the modal when clicking on the close button', () => {
                        UserEvent.click(screen.getByText(CLOSE_BUTTON))

                        expect(screen.queryByText(LOGIN_FAILED_MODAL_TITLE)).not.toBeInTheDocument()
                        expect(screen.queryByText(LOGIN_FAILED_MODAL_MESSAGE)).not.toBeInTheDocument()
                    })
                })

                describe('given the error occurred for any other reason', () => {
                    beforeEach(async () => {
                        const apiError: APIError = new APIError(404, "Any other code", "Some error message")
                        loginApiRequest.mockReturnValue({ 
                            token: undefined,
                            apiError 
                        })
                        renderComponent()
                        
                        UserEvent.type(screen.getByPlaceholderText(EMAIL_PLACEHOLDER), EMAIL)
                        UserEvent.type(screen.getByPlaceholderText(PASSWORD_PLACEHOLDER), PASSWORD)
                        await act(async () => {
                            UserEvent.click(screen.getByText(LOGIN_BUTTON))
                        })
                    })

                    it('should show the something went wrong modal', () => {
                        expect(screen.getByText(SOMETHING_WENT_WRONG_MODAL_TITLE)).toBeInTheDocument()
                        expect(screen.getByText(SOMETHING_WENT_WRONG_MODAL_MESSAGE)).toBeInTheDocument()
                    })

                    it('should close the modal when clicking on the close button', () => {
                        UserEvent.click(screen.getByText(CLOSE_BUTTON))

                        expect(screen.queryByText(SOMETHING_WENT_WRONG_MODAL_TITLE)).not.toBeInTheDocument()
                        expect(screen.queryByText(SOMETHING_WENT_WRONG_MODAL_MESSAGE)).not.toBeInTheDocument()
                    })
                })
            })
        })
    })

    const renderComponent = async () => {
        jest.spyOn(useAuthenticationRequests, "default").mockReturnValue({
            loginApiRequest
        })  

        jest.spyOn(useTokenCookie, "default").mockReturnValue({
            token,
            tokenLoading: false,
            setTokenCookie,
            removeTokenCookie: jest.fn()
        }) 

        render(
            <BrowserRouter>
                <I18nextProvider i18n={I18n}>
                    <Login />
                </I18nextProvider>
            </BrowserRouter>
        )
    }
})