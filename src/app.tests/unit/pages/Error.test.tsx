import { render, screen } from "@testing-library/react"
import { I18nextProvider } from 'react-i18next'
import { BrowserRouter } from "react-router-dom"
import * as useRedirection from "app/hooks/redirections/useRedirection"
import Error from "app/pages/Error"
import I18n from "i18n.js"
import UserEvent from "@testing-library/user-event"

describe('Error component', () => {
    const RETURN_BUTTON: string = "Return"

    const redirectToPrecedentPage = jest.fn()

    beforeEach(() => {
        jest.spyOn(useRedirection, "default").mockReturnValue({
            redirectToPrecedentPage,
            redirectToLogin: jest.fn(),
            redirectToDashboard: jest.fn(),
            redirectToSubscribe: jest.fn(),
            redirectToForgotPassword: jest.fn(),
            redirectToError: jest.fn()
        })  
        
        render(
            <BrowserRouter>
                <I18nextProvider i18n={I18n}>
                    <Error />
                </I18nextProvider>
            </BrowserRouter>
        )
    })
    
    it('should display a title', () => {
        expect(screen.getByText("Something went wrong...")).toBeInTheDocument()
    })

    it('should display some text', () => {
        expect(screen.getByText("We are very sorry about this. Our team has been informed and we will correct the problem as soon as possible.")).toBeInTheDocument()
        expect(screen.getByText("In the meantime, you can try again, or")).toBeInTheDocument()
        expect(screen.getByText("contact us here.")).toBeInTheDocument()
    })

    it('should display a return button', () => {
        expect(screen.getByText(RETURN_BUTTON)).toBeInTheDocument()
    })

    it('should call the redirectToPrecedentPage hook method when clicking on the button', () => {
        UserEvent.click(screen.getByText(RETURN_BUTTON))

        expect(redirectToPrecedentPage).toHaveBeenCalledTimes(1)
    })
})