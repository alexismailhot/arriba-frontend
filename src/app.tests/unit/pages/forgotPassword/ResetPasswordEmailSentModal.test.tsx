import { render, screen } from "@testing-library/react"
import { I18nextProvider } from 'react-i18next'
import { act } from "react-dom/test-utils"
import * as useRedirection from "app/hooks/redirections/useRedirection"
import ResetPasswordEmailSentModal from "app/pages/forgotPassword/ResetPasswordEmailSentModal"
import I18n from "i18n.js"
import userEvent from "@testing-library/user-event"

const MOCKED_LOAD_ANIMATION: string = 'Mocked load animation'
jest.mock('app/components/common/LoadAnimation', () => {
    return {
        __esModule: true,
        default: () => (
            <p>{MOCKED_LOAD_ANIMATION}</p>
        )
    }
})

describe('ResetPasswordEmailSentModal component', () => {
    const EMAIL: string = "some_email@gmail.ca"
    const CLOSE_BUTTON: string = "Close"
    const NO_EMAIL_RECEIVED: string = "No email received?"
    const SEND_AGAIN_LINK: string = "Send again"
    const WE_SENT_A_NEW_EMAIL: string = `We have just sent a new email to ${EMAIL}. Please check your spam folder as well.`

    const resendEmail = jest.fn()
    const redirectToLogin = jest.fn()

    beforeEach(() => {
        jest.spyOn(useRedirection, "default").mockReturnValue({
            redirectToPrecedentPage: jest.fn(),
            redirectToLogin,
            redirectToDashboard: jest.fn(),
            redirectToSubscribe: jest.fn(),
            redirectToForgotPassword: jest.fn(),
            redirectToError: jest.fn()
        })  
        
        render(
            <I18nextProvider i18n={I18n}>
                <ResetPasswordEmailSentModal 
                    email={EMAIL}  
                    resendEmail={resendEmail}
                />
            </I18nextProvider>
        )
    })

    it('should display a title', () => {
        expect(screen.getByText("We sent a reset link your way!")).toBeInTheDocument()
    })

    it('should explain that an email should be received soon', () => {
        expect(screen.getByText("You should receive an email in the next few seconds.")).toBeInTheDocument()
    })

    it('should show a link to resend email if not received', () => {
        expect(screen.getByText(NO_EMAIL_RECEIVED)).toBeInTheDocument()
        expect(screen.getByText(SEND_AGAIN_LINK)).toBeInTheDocument()
    })

    it('should not tell the user that an email has been resent', async () => {
        expect(screen.queryByText(WE_SENT_A_NEW_EMAIL)).not.toBeInTheDocument()
    })

    describe('when clicking on the link to resend email', () => {
        it('should hide the link to resend email', async () => {
            await act(async () => {
                userEvent.click(screen.getByText(SEND_AGAIN_LINK))
            })
            
            expect(screen.queryByText(NO_EMAIL_RECEIVED)).not.toBeInTheDocument()
            expect(screen.queryByText(SEND_AGAIN_LINK)).not.toBeInTheDocument()
        })

        it('should tell the user that an email has been resent', async () => {
            await act(async () => {
                userEvent.click(screen.getByText(SEND_AGAIN_LINK))
            })

            expect(screen.getByText(WE_SENT_A_NEW_EMAIL)).toBeInTheDocument()
        })
    })

    it('should show a close button', () => {
        expect(screen.getByText(CLOSE_BUTTON)).toBeInTheDocument()
    })

    it('should call the redirectToLogin hook method when clicking on the close button', () => {
        userEvent.click(screen.getByText(CLOSE_BUTTON))

        expect(redirectToLogin).toHaveBeenCalledTimes(1)
    })
})