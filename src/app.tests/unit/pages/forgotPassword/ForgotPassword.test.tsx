import { render, screen } from "@testing-library/react"
import { I18nextProvider } from 'react-i18next'
import { ResetPasswordEmailSentModalProps } from "app/pages/forgotPassword/ResetPasswordEmailSentModal"
import { act } from "react-dom/test-utils"
import * as useUserRequests from "app/hooks/API/useUserRequest"
import I18n from "i18n.js"
import ForgotPassword from "app/pages/forgotPassword/ForgotPassword"
import userEvent from "@testing-library/user-event"
import ForgotPasswordRequest from "app/infra/http/requests/ForgotPasswordRequest"
import APIError from "app/infra/http/APIError"
import StatusCode from "app/infra/http/StatusCode"

const MOCKED_RESET_PASSWORD_EMAIL_SENT_MODAL: string = 'Mocked reset password email sent modal'
const RESEND_EMAIL_FROM_MODAL: string = "Resend email"
jest.mock('app/pages/forgotPassword/ResetPasswordEmailSentModal', () => {
    return {
        __esModule: true,
        default: ({ resendEmail }: ResetPasswordEmailSentModalProps) => (
            <div>
                <p>{MOCKED_RESET_PASSWORD_EMAIL_SENT_MODAL}</p>
                <div data-testid={RESEND_EMAIL_FROM_MODAL}  onClick={resendEmail} />
            </div>
        )
    }
})

describe('ForgotPassword component', () => {
    const ERROR_MODAL_TITLE: string = "We were unable to send the reset link..."
    const EMAIL_ERROR_MESSAGE: string = "Email must not be empty."
    const EMAIL_INPUT_PLACEHOLDER: string = "Email"
    const SEND_BUTTON: string = "Send"
    const CLOSE_BUTTON: string = "Close"
    const EMAIL: string = "some_email@gmail.ca"

    const forgotPasswordApiRequest = jest.fn()

    beforeEach(() => {
        forgotPasswordApiRequest.mockReturnValue({})
    })

    it('should not show modals by default', () => {
        renderComponent()

        expect(screen.queryByText(MOCKED_RESET_PASSWORD_EMAIL_SENT_MODAL)).not.toBeInTheDocument()
        expect(screen.queryByText(ERROR_MODAL_TITLE)).not.toBeInTheDocument()
    })

    it('should not show error message by default', () => {
        renderComponent()

        expect(screen.queryByText(EMAIL_ERROR_MESSAGE)).not.toBeInTheDocument()
    })

    it('should show the page title', () => {
        renderComponent()

        expect(screen.getByText("Forgot your password?")).toBeInTheDocument()
    })

    it('should tell the user to enter his email', () => {
        renderComponent()

        expect(screen.getByText("Please enter your email address below and we will send you a reset link.")).toBeInTheDocument()
    })

    it('should show an email input', () => {
        renderComponent()

        expect(screen.getByPlaceholderText(EMAIL_INPUT_PLACEHOLDER).closest("input")).toHaveAttribute("type", "text")
    })

    it('should show a send button', () => {
        renderComponent()

        expect(screen.getByText(SEND_BUTTON).closest("button")).toHaveAttribute("type", "button")
    })

    describe('given the send button is clicked', () => {
        describe('given the email input is empty', () => {
            it('should show the email error message', async () => {
                renderComponent()

                await act(async () => {
                    userEvent.click(screen.getByText(SEND_BUTTON))
                })
                
                expect(screen.getByText(EMAIL_ERROR_MESSAGE)).toBeInTheDocument()
            })

            it('should hide the error message as soon as the user starts typing in the email input', async () => {
                renderComponent()
                await act(async () => {
                    userEvent.click(screen.getByText(SEND_BUTTON))
                })

                userEvent.type(screen.getByPlaceholderText(EMAIL_INPUT_PLACEHOLDER), 'a')

                expect(screen.queryByText(EMAIL_ERROR_MESSAGE)).not.toBeInTheDocument()
            })
        })

        describe('given the email input has been filled', () => {
            it('should call the forgotPasswordApiRequest hook method', async () => {
                renderComponent()
                userEvent.type(screen.getByPlaceholderText(EMAIL_INPUT_PLACEHOLDER), EMAIL)
                const expectedRequest: ForgotPasswordRequest = { email: EMAIL }
                
                await act(async () => {
                    userEvent.click(screen.getByText(SEND_BUTTON))
                })

                expect(forgotPasswordApiRequest).toHaveBeenCalledTimes(1)
                expect(forgotPasswordApiRequest).toHaveBeenCalledWith(expectedRequest)
            })

            describe('given the forgotPasswordApiRequest hook method succeeds', () => {
                beforeEach(() => {
                    forgotPasswordApiRequest.mockReturnValue({})
                })

                it('should open the email sent modal', async () => {
                    renderComponent()
                    userEvent.type(screen.getByPlaceholderText(EMAIL_INPUT_PLACEHOLDER), EMAIL)
                    
                    await act(async () => {
                        userEvent.click(screen.getByText(SEND_BUTTON))
                    })

                    expect(screen.getByText(MOCKED_RESET_PASSWORD_EMAIL_SENT_MODAL)).toBeInTheDocument()
                })

                describe('given the user resends the email from the modal', () => {
                    it('should call the forgotPasswordApiRequest hook method for a second time', async () => {
                        renderComponent()
                        userEvent.type(screen.getByPlaceholderText(EMAIL_INPUT_PLACEHOLDER), EMAIL)
                        const expectedRequest: ForgotPasswordRequest = { email: EMAIL }
                        await act(async () => {
                            userEvent.click(screen.getByText(SEND_BUTTON))
                        })

                        await act(async () => {
                            userEvent.click(screen.getByTestId(RESEND_EMAIL_FROM_MODAL))
                        })
                        
                        expect(forgotPasswordApiRequest).toHaveBeenCalledTimes(2)
                        expect(forgotPasswordApiRequest).toHaveBeenCalledWith(expectedRequest)
                    })
                })
            })

            describe('given the forgotPasswordApiRequest hook method fails', () => {
                beforeEach(() => {
                    const apiError: APIError = new APIError(StatusCode.Unauthorized, undefined, undefined)
                    forgotPasswordApiRequest.mockReturnValue({ apiError })
                })
                
                it('should open the error modal', async () => {
                    renderComponent()
                    userEvent.type(screen.getByPlaceholderText(EMAIL_INPUT_PLACEHOLDER), EMAIL)
                    
                    await act(async () => {
                        userEvent.click(screen.getByText(SEND_BUTTON))
                    })

                    expect(screen.getByText(ERROR_MODAL_TITLE)).toBeInTheDocument()
                    expect(screen.getByText("Please ensure that the email address you provided is correct and try again.")).toBeInTheDocument()
                    expect(screen.getByText("If the problem persists, you can")).toBeInTheDocument()
                    expect(screen.getByText("contact us here.")).toBeInTheDocument()
                })

                it('should close the modal when the user clicks on the close button', async () => {
                    renderComponent()
                    userEvent.type(screen.getByPlaceholderText(EMAIL_INPUT_PLACEHOLDER), EMAIL)
                    await act(async () => {
                        userEvent.click(screen.getByText(SEND_BUTTON))
                    })

                    userEvent.click(screen.getByText(CLOSE_BUTTON))

                    expect(screen.queryByText(ERROR_MODAL_TITLE)).not.toBeInTheDocument()
                })
            })
        })
    })

    const renderComponent = (): void => {
        jest.spyOn(useUserRequests, "default").mockReturnValue({
            getProfileApiRequest: jest.fn(), 
            forgotPasswordApiRequest
        })  

        render(
            <I18nextProvider i18n={I18n}>
                <ForgotPassword />
            </I18nextProvider>
        )
    }
})