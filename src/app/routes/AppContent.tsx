import { useEffect, useState } from "react"
import { Navigate, Route, Routes } from "react-router-dom"
import useTokenCookie from "app/hooks/cookies/useTokenCookie"
import useRedirection from "app/hooks/redirections/useRedirection"
import Dashboard from "app/pages/Dashboard"
import Login from "app/pages/Login"
import NotFound from "app/pages/NotFound"
import Subscribe from "app/pages/Subscribe"
import AppPath from "app/routes/AppPath"
import Error from "app/pages/Error"
import PageWrap from "app/components/common/PageWrap"
import ForgotPassword from "app/pages/forgotPassword/ForgotPassword"
import ResetPassword from "app/pages/ResetPassword"

const AppContent = () => {
    return (
        <PageWrap>
            <Routes>
                <Route path={AppPath.Login} element={<Login />} />
                <Route path={AppPath.Subscribe} element={<Subscribe />} />
                <Route path={AppPath.ForgotPassword} element={<ForgotPassword />} />
                <Route path={AppPath.ResetPassword} element={<ResetPassword />} />
                <Route path={AppPath.Error} element={<Error />} />

                <Route path={AppPath.Dashboard} element={
                    <RequireAuth>
                        <Dashboard />
                    </RequireAuth>
                } />
                
                <Route path="*" element={<NotFound />} />
            </Routes>
        </PageWrap>
    )
}

const RequireAuth: React.FC = ({ children }) => {
    const { redirectToLogin } = useRedirection()
    const { token, tokenLoading } = useTokenCookie()

    const [isAuthenticated, setIsAuthenticated] = useState<boolean>(true)
    
    useEffect(() => {
        if (!tokenLoading && token === undefined) {
            setIsAuthenticated(false)
        }
    }, [token, tokenLoading, redirectToLogin])

    return isAuthenticated ? (children as React.ReactElement) : <Navigate to={AppPath.Login} />
}

export default AppContent