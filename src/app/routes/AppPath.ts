enum AppPath {
    Login = "/",
    Dashboard = "/dashboard",
    Subscribe = "/subscribe",
    ForgotPassword = "/forgotPassword",
    ResetPassword = "/resetPassword",
    Error = "/error"
}

export default AppPath