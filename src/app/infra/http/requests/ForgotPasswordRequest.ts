interface ForgotPasswordRequest {
    email: string
}

export default ForgotPasswordRequest