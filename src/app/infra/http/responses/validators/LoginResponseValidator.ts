import LoginResponse from "app/infra/http/responses/LoginResponse"

class LoginResponseValidator {
    static validate(loginResponse: LoginResponse): void {
        if (typeof loginResponse.token !== "string") {
            throw new Error(`The LoginResponse token field should be a string, received ${typeof loginResponse.token}.`)
        }
    }
}

export default LoginResponseValidator