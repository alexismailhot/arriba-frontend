import ProfileResponse from "app/infra/http/responses/ProfileResponse"

class ProfileResponseValidator {
    static validate(profileResponse: ProfileResponse): void {
        if (typeof profileResponse.email !== "string") {
            throw new Error(`The ProfileResponse email field should be a string, received ${typeof profileResponse.email}.`)
        } else if (typeof profileResponse.firstName !== "string") {
            throw new Error(`The ProfileResponse firstName field should be a string, received ${typeof profileResponse.firstName}.`)
        }
    }
}

export default ProfileResponseValidator