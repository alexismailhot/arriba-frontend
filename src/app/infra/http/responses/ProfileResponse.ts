interface ProfileResponse {
    email: string
    firstName: string
}

export default ProfileResponse