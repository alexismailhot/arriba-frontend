import StatusCode from "app/infra/http/StatusCode"

class APIError {
    private readonly INVALID_EMAIL_CODE = "invalid_email"
    private readonly INVALID_PASSWORD_CODE = "incorrect_password"
    private readonly INVALID_USERNAME_CODE = "invalid_username"

    public readonly status: number
    public readonly code: string
    public readonly message: string
    
    
    public constructor(status: number, code: string | undefined, message: string | undefined) {
        this.status = status
        this.code = code === undefined ? "" : code
        this.message = message === undefined ? "" : message
    }

    public isUnauthorized(): boolean {
        return this.status === StatusCode.Forbidden || this.status === StatusCode.Unauthorized
    }

    public isInvalidEmailCode(): boolean {
        return this.codeContainsSubstring(this.INVALID_EMAIL_CODE)
    }

    public isInvalidPasswordCode(): boolean {
        return this.codeContainsSubstring(this.INVALID_PASSWORD_CODE)
    }

    public isInvalidUsernameCode(): boolean {
        return this.codeContainsSubstring(this.INVALID_USERNAME_CODE)
    }

    private codeContainsSubstring(substring: string): boolean {
        return this.code.includes(substring)
    }
}

export default APIError