class APIRoute {
    static login(): string {
        return "/jwt-auth/v1/token"
    }

    static profile(): string {
        return "/users/profile"
    }

    static forgotPassword(): string {
        return "/users/forgotPassword"
    }
}

export default APIRoute