class Http {
    static async postRequest(path: string, body: {}): Promise<Response> {
        const response = await fetch(
            this.buildRoute(path),
            {
                method: 'POST',
                headers: { 
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(body),
                credentials: 'include',
                mode: 'cors'
            }
        )

        return response
    }

    static async authenticatedPostRequest(path: string, body: {}, token: string): Promise<Response> {
        const response = await fetch(
            this.buildRoute(path),
            {
                method: 'POST',
                headers: { 
                    'Content-type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(body),
                credentials: 'include',
                mode: 'cors'
            }
        )

        return response
    }

    static async authenticatedGetRequest(path: string, token: string): Promise<Response> {
        const response = await fetch(
            this.buildRoute(path),
            {
                method: 'GET',
                headers: { 
                    'Authorization': `Bearer ${token}`
                },
                credentials: 'include',
                mode: 'cors'
            }
        )

        return response
    }

    private static buildRoute(path: string) {
        return `${process.env.REACT_APP_API_URL}${path}`
    }
}

export default Http