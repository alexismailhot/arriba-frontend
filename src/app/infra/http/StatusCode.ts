enum StatusCode {
    Ok = 200,
    Unauthorized = 401,
    Forbidden = 403
}

export default StatusCode