import Profile from "app/domain/Profile"
import ProfileResponse from "app/infra/http/responses/ProfileResponse"

class ProfileFactory {
    static create(profileResponse: ProfileResponse): Profile {
        return {
            email: profileResponse.email,
            firstName: profileResponse.firstName
        }
    }
}

export default ProfileFactory