import { useState } from "react"
import { useTranslation } from "react-i18next"
import Button from "app/components/common/Button"
import CenteredContent from "app/components/common/CenteredContent"
import InputErrorMessage from "app/components/common/inputs/InputErrorMessage"
import InputWrapper from "app/components/common/inputs/InputWrapper"
import useUserRequests from "app/hooks/API/useUserRequest"
import useStringHelper from "app/hooks/useStringHelper"
import ForgotPasswordRequest from "app/infra/http/requests/ForgotPasswordRequest"
import ResetPasswordEmailSentModal from "app/pages/forgotPassword/ResetPasswordEmailSentModal"
import ErrorModal from "app/components/common/modals/ErrorModal"

const ForgotPassword = () => {
    const { t } = useTranslation()
    const { isEmptyString } = useStringHelper()
    const { forgotPasswordApiRequest } = useUserRequests()
    
    const [email, setEmail] = useState<string>('')
    const [emailError, setEmailError] = useState<boolean>(false)
    const [emailSentModalVisibility, setEmailSentModalVisibility] = useState<boolean>(false)
    const [isButtonDisabled, setButtonDisabled] = useState<boolean>(false)
    const [errorModalVisibility, setErrorModalVisibility] = useState<boolean>(false)

    const sendResetPasswordRequest = async (): Promise<void> => {
        if (isEmptyString(email)) {
            setEmailError(true)
            return
        }

        disableButton()

        const forgotPasswordRequest: ForgotPasswordRequest = { email }
        const { apiError } = await forgotPasswordApiRequest(forgotPasswordRequest)

        if (!apiError) {
            openEmailSentModal()
        } else {
            openErrorModal()
        }

        enableButton()
    }

    const onEmailChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setEmail(event.target.value)
        setEmailError(false)
    }

    const openEmailSentModal = (): void => {
        setEmailSentModalVisibility(true)
    }

    const disableButton = (): void => {
        setButtonDisabled(true)
    }

    const enableButton = (): void => {
        setButtonDisabled(false)
    }

    const openErrorModal = (): void => {
        setErrorModalVisibility(true)
    }

    const closeErrorModal = (): void => {
        setErrorModalVisibility(false)
    }
    
    return (
        <CenteredContent>
            {emailSentModalVisibility && (
                <ResetPasswordEmailSentModal 
                    email={email}
                    resendEmail={sendResetPasswordRequest}
                />
            )}

            {errorModalVisibility && (
                <ErrorModal 
                    title={t("we_were_unable_to_send_reset_link")} 
                    message={t("please_make_sure_email_provided_is_correct_and_try_again")}
                    buttonText={t('close')} 
                    onClose={closeErrorModal} 
                    showContactUsText={true}
                />
            )}
            
            <h1 className="text-center">
                {t('forgot_your_password_?')}
            </h1>

            <p className="mb-2">
                {t("please_enter_email_and_we_will_send_resend_link")}
            </p>

            <InputWrapper>
                <input 
                    className="mt-6"
                    placeholder="Email"
                    type="text" 
                    value={email} 
                    onChange={event => onEmailChange(event)} 
                />
                {emailError && (
                    <InputErrorMessage text={t('email_must_not_be_empty')} />
                )}
            </InputWrapper>
            
            <Button 
                className="mt-8 test"
                content={t("send")} 
                purpose={"primary"}    
                onClick={sendResetPasswordRequest}
                disabled={isButtonDisabled}
            />
        </CenteredContent>
    )
}

export default ForgotPassword