import { useState } from "react"
import { useTranslation } from "react-i18next"
import Button from "app/components/common/Button"
import Modal from "app/components/common/modals/Modal"
import Emoji from "app/components/common/Emoji"
import useRedirection from "app/hooks/redirections/useRedirection"
import LoadAnimation from "app/components/common/LoadAnimation"
import CustomLink from "app/components/common/links/CustomLink"

export interface ResetPasswordEmailSentModalProps {
    email: string
    resendEmail: () => Promise<void>
}

const ResetPasswordEmailSentModal:React.FC<ResetPasswordEmailSentModalProps> = ({ email, resendEmail }) => {
    const { t } = useTranslation()
    const { redirectToLogin } = useRedirection()
    
    const [emailResent, setEmailResent] = useState<boolean>(false)
    const [linkClicked, setLinkClicked] = useState<boolean>(false)

    const sendEmailAgain = async (): Promise<void> => {
        setLinkClicked(true)
        
        await resendEmail()
        
        setEmailResent(true)
    }

    return (
        <Modal>
            <h2>{t("we_sent_a_resent_link_your_way")} <Emoji type="envelope-with-downwards-arrow-above" /></h2>

            <p className="mb-5">{t("you_should_receive_an_email_in_the_next_few_seconds")}</p>

            {emailResent === false ? (
                <div className="flex flex-col items-center">
                    <p className="text-sm">
                        {t("no_email_received_?")}
                    </p>
                    <div className="flex items-center">
                        <CustomLink 
                            text={t("send_again")} 
                            onClick={sendEmailAgain} 
                            disabled={linkClicked} 
                        />
                        {linkClicked && (
                            <LoadAnimation color="blue" className="ml-2" />
                        )}
                    </div>
                </div>
            ) : (
                <p className="text-sm mt-4">
                    {t("we_have_just_sent_a_new_email_check_spam", { email })}
                </p>
            )}

            <Button
                className="mt-5"
                content={t("close")}
                purpose='primary'
                onClick={redirectToLogin}
            />
        </Modal>
    )
}

export default ResetPasswordEmailSentModal