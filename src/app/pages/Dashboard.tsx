import { useEffect, useState } from "react"
import * as Sentry from "@sentry/react"
import useUserRequests from "app/hooks/API/useUserRequest"
import useTokenCookie from "app/hooks/cookies/useTokenCookie"
import useRedirection from "app/hooks/redirections/useRedirection"

// TODO: Implement this page (the current code was only to test that authenticated requests to the API are successful)
// TODO: Unit tests
const Dashboard = () => {
    const { token, removeTokenCookie } = useTokenCookie()
    const { getProfileApiRequest } = useUserRequests()
    const { redirectToError } = useRedirection()

    const [email, setEmail] = useState<string>('')
    const [firstName, setFirstName] = useState<string>('')

    useEffect(() => {
        const getProfile = async (token: string): Promise<void> => {
            const { profile, apiError } = await getProfileApiRequest(token)

            if (profile) {
                setEmail(profile.email)
                setFirstName(profile.firstName)
            } else if (apiError) {
                if (apiError.isUnauthorized()) {
                    removeTokenCookie()
                } else {
                    Sentry.captureMessage(`Backend error while getting profile in Dashboard. Status code: ${apiError.status}. Message: ${apiError.message}`)

                    redirectToError()
                }
            }
        }

        if (token) {
            getProfile(token)
        }
    }, [
        token, 
        getProfileApiRequest, 
        redirectToError,
        removeTokenCookie
    ])

    return (
        <div>
            <h1>Dashboard</h1>
            <p>Email: {email}</p>
            <p>First name: {firstName}</p>
            <button onClick={removeTokenCookie}>Sign out</button>
        </div>
    )
}

export default Dashboard