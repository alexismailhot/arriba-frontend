import { useTranslation } from "react-i18next"
import CenteredContent from "app/components/common/CenteredContent"
import Emoji from "app/components/common/Emoji"
import Button from "app/components/common/Button"
import MailToLink from "app/components/common/links/MailToLink"
import useRedirection from "app/hooks/redirections/useRedirection"

const Error = () => {
    const { t } = useTranslation()
    const { redirectToPrecedentPage } = useRedirection()
    
    return (
        <CenteredContent>
            <div className="text-center">
                <h1>{t('something_went_wrong')}... <Emoji type='confounded' /></h1>

                <p className="mb-2">
                    {t("we_are_very_sorry_about_that_our_team_has_been_informed")}
                </p>
                <p>
                    {t("in_the_meantime_you_can_try_again_or")} 
                    <MailToLink 
                        linkText={t("contact_us_here")} 
                        email={process.env.REACT_APP_SUPPORT_EMAIL!} 
                        subject={t("website_problem_arriba_flights")}
                        body={t("problem_occurred_while_i_was_trying_to")}
                    />
                </p>

                <Button 
                    className="mt-8"
                    content={t("return")} 
                    purpose={"primary"}    
                    onClick={redirectToPrecedentPage}        
                />
            </div>
        </CenteredContent>
    )
}

export default Error