import { useEffect, useState } from "react"
import { useTranslation } from "react-i18next"
import * as Sentry from "@sentry/react"
import useAuthenticationRequests from "app/hooks/API/useAuthenticationRequests"
import useTokenCookie from "app/hooks/cookies/useTokenCookie"
import useRedirection from "app/hooks/redirections/useRedirection"
import LoginRequest from "app/infra/http/requests/LoginRequest"
import Emoji from "app/components/common/Emoji"
import CenteredContent from "app/components/common/CenteredContent"
import Button from "app/components/common/Button"
import useStringHelper from "app/hooks/useStringHelper"
import InputWrapper from "app/components/common/inputs/InputWrapper"
import InputErrorMessage from "app/components/common/inputs/InputErrorMessage"
import ErrorModal from "app/components/common/modals/ErrorModal"
import CustomLink from "app/components/common/links/CustomLink"

const Login = () => {
    const { loginApiRequest } = useAuthenticationRequests()
    const { token, setTokenCookie } = useTokenCookie()
    const { redirectToDashboard, redirectToSubscribe, redirectToForgotPassword } = useRedirection()
    const { isEmptyString } = useStringHelper()
    const { t } = useTranslation()

    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [buttonDisabled, setButtonDisabled] = useState<boolean>(false)
    const [emailError, setEmailError] = useState<boolean>(false)
    const [passwordError, setPasswordError] = useState<boolean>(false)
    const [errorModalVisibility, setErrorModalVisibility] = useState<boolean>(false)
    const [errorModalTitle, setErrorModalTitle] = useState<string>('')
    const [errorModalText, setErrorModalText] = useState<string>('')

    useEffect(() => {
        if (token) {
            redirectToDashboard()
        }
    }, [token, redirectToDashboard])

    const signIn = async (): Promise<void> => {
        disableLoginButton()

        const isFormValid = validateForm()
        if (!isFormValid) {
            enableLoginButton()
            return
        }

        const loginRequest: LoginRequest = {
            username: email,
            password: password
        }

        const { token, apiError } = await loginApiRequest(loginRequest)

        if (token) {
            setTokenCookie(token)
            redirectToDashboard()
        } else if (apiError) {
            if (apiError.isInvalidEmailCode() || apiError.isInvalidPasswordCode() || apiError.isInvalidUsernameCode()) {
                setErrorModalTitle(t('login_failed'))
                setErrorModalText(t('the_email_or_password_you_entered_seems_invalid'))
                openErrorModal()
            } else {
                Sentry.captureMessage(`Backend error while loging in in Login. Status code: ${apiError.status}. Message: ${apiError.message}`)
                
                setErrorModalTitle(t('something_went_wrong'))
                setErrorModalText(t('there_seems_to_be_a_problem_on_our_end'))
                openErrorModal()
            }

            enableLoginButton()
        }
    }

    const validateForm = (): boolean => {
        let isValid: boolean = true

        if (isEmptyString(email)) {
            setEmailError(true)
            isValid = false
        }
        
        if (isEmptyString(password)) {
            setPasswordError(true)
            isValid = false
        }

        return isValid
    }

    const onEmailChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setEmail(event.target.value)
        setEmailError(false)
    }

    const onPasswordChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setPassword(event.target.value)
        setPasswordError(false)
    }

    const disableLoginButton = (): void => {
        setButtonDisabled(true)
    }

    const enableLoginButton = (): void => {
        setButtonDisabled(false)
    }

    const openErrorModal = (): void => {
        setErrorModalVisibility(true)
    }

    const closeErrorModal = (): void => {
        setErrorModalVisibility(false)
    }
    
    return (
        <CenteredContent>
            {errorModalVisibility && (
                <ErrorModal 
                    title={errorModalTitle} 
                    message={errorModalText}
                    buttonText={t('close')} 
                    onClose={closeErrorModal} 
                />
            )}

            <h1 className="text-center">
                {t('welcome_back_globetrotter')} <Emoji type='earth' />
            </h1>

            <InputWrapper>
                <input 
                    className="mt-6"
                    placeholder="Email"
                    type="text" 
                    value={email} 
                    onChange={event => onEmailChange(event)} 
                />
                {emailError && (
                    <InputErrorMessage text={t('email_must_not_be_empty')} />
                )}
            </InputWrapper>

            <InputWrapper>
                <input 
                    className="mt-4"
                    placeholder="Password"
                    type="password" 
                    value={password} 
                    onChange={event => onPasswordChange(event)} 
                />
                {passwordError && (
                    <InputErrorMessage text={t('password_must_not_be_empty')} />
                )}
            </InputWrapper>
            
            <Button 
                className="mt-10"
                content={t("login")} 
                onClick={signIn}
                purpose={"primary"}  
                disabled={buttonDisabled}         
            />

            <p 
                className="mt-6 text-center" 
                onClick={redirectToSubscribe}
            >
                {t('dont_have_an_account')} <span className="link">{t('subscribe')}</span>
            </p>

            <CustomLink 
                text={t('trouble_logging_in')} 
                className="mt-2 text-center" 
                onClick={redirectToForgotPassword}
            />
        </ CenteredContent>
    )
}

export default Login