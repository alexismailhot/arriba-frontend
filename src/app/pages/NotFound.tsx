import { useTranslation } from "react-i18next"
import CenteredContent from "app/components/common/CenteredContent"
import Emoji from "app/components/common/Emoji"
import Button from "app/components/common/Button"
import MailToLink from "app/components/common/links/MailToLink"
import useRedirection from "app/hooks/redirections/useRedirection"

const NotFound = () => {
    const { t } = useTranslation()
    const { redirectToPrecedentPage } = useRedirection()

    return (
        <CenteredContent>
            <div className="text-center">
                <h1>{t("looks_like_this_page_does_not_exist")} <Emoji type='thinking-face' /></h1>

                <p className="mb-2">
                    {t("please_make_sure_url_is_correct")}
                </p>
                <p>
                    {t("if_you_think_this_is_a_problem_on_our_part")}
                    <MailToLink 
                        linkText={t("contact_us_here")} 
                        email={process.env.REACT_APP_SUPPORT_EMAIL!} 
                        subject={t("page_not_found_on_arriba_flights")}
                        body={t("i_am_unable_to_find_the_page_can_you_help_me", { pageUrl: window.location.href })}
                    />
                </p>

                <Button 
                    className="mt-8"
                    content={t("return")} 
                    purpose={"primary"}    
                    onClick={redirectToPrecedentPage}        
                />
            </div>
        </CenteredContent>
    )
}

export default NotFound