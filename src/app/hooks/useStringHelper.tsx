import { useCallback } from "react"

const useStringHelper = () => {
    const isEmptyString = useCallback((string: string) => {
        return !string || !string.trim()
    }, [])

    return { isEmptyString }
}

export default useStringHelper