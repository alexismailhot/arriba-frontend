import { useCallback } from "react"
import I18next from "i18next"
import Language from "app/hooks/language/Language"

const useLanguageHelper = () => {
    const getCurrentLanguage = useCallback((): string => {
        return I18next.language
    }, [])

    const getOppositeLanguage = useCallback((): string => {
        return I18next.language === Language.EN ? Language.FR : Language.EN
    }, [])

    const changeLanguage = useCallback((): void => {
        I18next.changeLanguage(getOppositeLanguage())
    }, [getOppositeLanguage])

    return {
        getCurrentLanguage,
        getOppositeLanguage,
        changeLanguage
    }
}

export default useLanguageHelper