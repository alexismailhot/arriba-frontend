import { useCallback, useEffect, useState } from "react"
import { useCookies } from "react-cookie"
import { CookieSetOptions } from "universal-cookie"
import Cookie from "app/hooks/cookies/Cookie"

const cookieSetOptions: CookieSetOptions = {
    path: "/",
    secure: true,
    maxAge: 300, // 5 minutes
    sameSite: 'strict'
}

const useTokenCookie = () => {
    const [cookies, setCookie, removeCookie] = useCookies([Cookie.Token])

    const [token, setToken] = useState<string | undefined>(cookies[Cookie.Token])
    const [tokenLoading, setTokenLoading] = useState<boolean>(true)

    useEffect(() => {
        setToken(cookies[Cookie.Token])
        setTokenLoading(false)
    }, [cookies])

    const setTokenCookie = useCallback((token: string): void => {
        setCookie(Cookie.Token, token, cookieSetOptions)
    }, [setCookie])

    const removeTokenCookie = useCallback((): void => {
        removeCookie(Cookie.Token, cookieSetOptions)
    }, [removeCookie])

    return { token, tokenLoading, setTokenCookie, removeTokenCookie }
}

export default useTokenCookie