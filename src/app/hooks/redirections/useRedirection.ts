import { useCallback } from "react"
import { useNavigate } from "react-router-dom"
import AppPath from "app/routes/AppPath"

const useRedirection = () => {
    const navigate = useNavigate()

    const redirectToPrecedentPage = useCallback(() => {
        navigate(-1)
    }, [])

    const redirectToLogin = useCallback(() => {
        navigate(AppPath.Login)
    }, [navigate])

    const redirectToDashboard = useCallback(() => {
        navigate(AppPath.Dashboard)
    }, [navigate])

    const redirectToSubscribe = useCallback(() => {
        navigate(AppPath.Subscribe)
    }, [navigate])

    const redirectToForgotPassword = useCallback(() => {
        navigate(AppPath.ForgotPassword)
    }, [navigate])

    const redirectToError = useCallback(() => {
        navigate(AppPath.Error)
    }, [navigate])

    return { 
        redirectToPrecedentPage,
        redirectToLogin, 
        redirectToDashboard, 
        redirectToSubscribe, 
        redirectToForgotPassword,
        redirectToError 
    }
}

export default useRedirection