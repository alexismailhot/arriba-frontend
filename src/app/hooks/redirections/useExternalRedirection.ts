import { useCallback } from "react"
import ExternalUrl from "app/hooks/redirections/ExternalUrl"
import Language from "app/hooks/language/Language"
import useLanguageHelper from "app/hooks/language/useLanguageHelper"

const useExternalRedirection = () => {
    const { getCurrentLanguage } = useLanguageHelper()

    const redirectToHome = useCallback(() => {
        const currentLanguage: string = getCurrentLanguage()
        const path: string = currentLanguage === Language.FR ? ExternalUrl.HomeFr : ExternalUrl.HomeEn

        window.location.href = `${process.env.REACT_APP_WEBSITE_URL}${path}`
    }, [getCurrentLanguage])

    const redirectToFAQ = useCallback(() => {
        const currentLanguage: string = getCurrentLanguage()
        const path: string = currentLanguage === Language.FR ? ExternalUrl.FaqFr : ExternalUrl.FaqEn

        window.location.href = `${process.env.REACT_APP_WEBSITE_URL}${path}`
    }, [getCurrentLanguage])

    const redirectToAboutUs = useCallback(() => {
        const currentLanguage: string = getCurrentLanguage()
        const path: string = currentLanguage === Language.FR ? ExternalUrl.AboutUsFr : ExternalUrl.AboutUsEn

        window.location.href = `${process.env.REACT_APP_WEBSITE_URL}${path}`
    }, [getCurrentLanguage])

    const redirectToTermsAndConditions = useCallback(() => {
        const currentLanguage: string = getCurrentLanguage()
        const path: string = currentLanguage === Language.FR ? ExternalUrl.TermsAndConditionsFr : ExternalUrl.TermsAndConditionsEn

        window.location.href = `${process.env.REACT_APP_WEBSITE_URL}${path}`
    }, [getCurrentLanguage])

    const redirectToPrivacyPolicy = useCallback(() => {
        const currentLanguage: string = getCurrentLanguage()
        const path: string = currentLanguage === Language.FR ? ExternalUrl.PrivacyPolicyFr : ExternalUrl.PrivacyPolicyEn

        window.location.href = `${process.env.REACT_APP_WEBSITE_URL}${path}`
    }, [getCurrentLanguage])

    const redirectToInstagramProfile = useCallback(() => {
        window.open(ExternalUrl.InstagramProfile, "_blank")
    }, [])

    const redirectToFacebookProfile = useCallback(() => {
        window.open(ExternalUrl.FacebookProfile, "_blank")
    }, [])

    const redirectToTwitterProfile = useCallback(() => {
        window.open(ExternalUrl.TwitterProfile, "_blank")
    }, [])

    return { 
        redirectToHome, 
        redirectToFAQ, 
        redirectToAboutUs, 
        redirectToTermsAndConditions, 
        redirectToPrivacyPolicy,
        redirectToInstagramProfile,
        redirectToFacebookProfile,
        redirectToTwitterProfile
    }
}

export default useExternalRedirection