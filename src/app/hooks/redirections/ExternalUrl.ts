enum ExternalUrl {
    HomeEn = '',
    HomeFr = '/fr/accueil',
    FaqEn = '/questions',
    FaqFr = '/fr/faq',
    AboutUsEn = '/about-us',
    AboutUsFr = '/fr/a-propos-de-nous',
    TermsAndConditionsFr = '/fr/termes-et-conditions',
    TermsAndConditionsEn = '/terms-and-conditions',
    PrivacyPolicyFr = '/fr/politique-de-confidentialite',
    PrivacyPolicyEn = '/privacy-policy',
    InstagramProfile = 'https://www.instagram.com/arribaflights',
    FacebookProfile = 'https://www.facebook.com/arribaflights',
    TwitterProfile = 'https://twitter.com/arriba_flights'
}

export default ExternalUrl