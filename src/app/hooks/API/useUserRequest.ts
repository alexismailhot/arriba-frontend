import { useCallback } from "react"
import * as Sentry from "@sentry/react"
import ProfileFactory from "app/domain/factories/ProfileFactory"
import Profile from "app/domain/Profile"
import APIRoute from "app/infra/http/APIRoute"
import ProfileResponse from "app/infra/http/responses/ProfileResponse"
import ProfileResponseValidator from "app/infra/http/responses/validators/ProfileResponseValidator"
import StatusCode from "app/infra/http/StatusCode"
import APIError from "app/infra/http/APIError"
import Http from "app/infra/http/Http"
import useRedirection from "app/hooks/redirections/useRedirection"
import ForgotPasswordRequest from "app/infra/http/requests/ForgotPasswordRequest"

const useUserRequests = () => {
    const { redirectToError } = useRedirection()
    
    const getProfileApiRequest = useCallback(async (token: string): Promise<{ profile?: Profile, apiError?: APIError }> => {
        const response: Response = await Http.authenticatedGetRequest(APIRoute.profile(), token)
        const jsonResponse: ProfileResponse = await response.json()
        const status: number = response.status

        if (status === StatusCode.Ok) {
            try {
                ProfileResponseValidator.validate(jsonResponse)
                const profile: Profile = ProfileFactory.create(jsonResponse)
    
                return { profile: profile, apiError: undefined }
            } catch (e: unknown) {
                Sentry.captureException(e)

                redirectToError()
                return {}
            }
        } else {
            const apiError: APIError = new APIError(status, (jsonResponse as any).code, (jsonResponse as any).message)

            return { profile: undefined, apiError: apiError }
        }
    }, [])

    const forgotPasswordApiRequest = useCallback(async (forgotPasswordRequest: ForgotPasswordRequest): Promise<{ apiError?: APIError }> => {
        const response: Response = await Http.postRequest(APIRoute.forgotPassword(), forgotPasswordRequest)
        const status: number = response.status

        if (status === StatusCode.Ok) {
            return {}
        } else {
            const jsonResponse: any = await response.json()
            const apiError: APIError = new APIError(status, jsonResponse.code, jsonResponse.message)

            return { apiError }
        }
    }, [])

    return { getProfileApiRequest, forgotPasswordApiRequest }
}

export default useUserRequests