import { useCallback } from "react"
import * as Sentry from "@sentry/react"
import useRedirection from "app/hooks/redirections/useRedirection"
import APIRoute from "app/infra/http/APIRoute"
import LoginRequest from "app/infra/http/requests/LoginRequest"
import LoginResponse from "app/infra/http/responses/LoginResponse"
import LoginResponseValidator from "app/infra/http/responses/validators/LoginResponseValidator"
import StatusCode from "app/infra/http/StatusCode"
import APIError from "app/infra/http/APIError"
import Http from "app/infra/http/Http"

const useAuthenticationRequests = () => {
    const { redirectToError } = useRedirection()

    const loginApiRequest = useCallback(async (loginRequest: LoginRequest): Promise<{ token?: string, apiError?: APIError }> => {
        const response: Response = await Http.postRequest(APIRoute.login(), loginRequest)
        const jsonResponse: LoginResponse = await response.json()
        const status: number = response.status

        if (status === StatusCode.Ok) {
            try {
                LoginResponseValidator.validate(jsonResponse)

                return { token: jsonResponse.token, apiError: undefined }
            } catch (e: unknown) {
                Sentry.captureException(e)

                redirectToError()
                return {}
            }
        } else {
            const apiError: APIError = new APIError(status, (jsonResponse as any).code, (jsonResponse as any).message)

            return { token: undefined, apiError: apiError }
        }
    }, [redirectToError])

    return { loginApiRequest }
}

export default useAuthenticationRequests