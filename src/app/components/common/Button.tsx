import LoadAnimation from "app/components/common/LoadAnimation"

interface Props {
    className?: string
    content: string
    purpose: "primary" | "secondary"
    disabled?: boolean
    onClick?: () => any
}

const Button: React.FC<Props> = ({ className = '', content, purpose, disabled = false, onClick }) => {
    return (
        <button
            type="button"
            className={`${className} ${purpose}`}
            disabled={disabled}
            onClick={onClick}
        >
            <div className="flex items-center">
                {disabled && (
                    <LoadAnimation color="white" className="mr-2" />
                )}
                {content}
            </div>
        </button>
    )
}

export default Button