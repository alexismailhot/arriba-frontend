interface Props {
    text: string
}

const InputErrorMessage: React.FC<Props> = ({ text }) => {
    return (
        <p className="input-error">
            {text}
        </p>
    )
}

export default InputErrorMessage