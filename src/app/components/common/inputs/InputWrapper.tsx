const InputWrapper: React.FC = ({ children }) => {
    return (
        <div className="input-wrapper">
            {children}
        </div>
    )
}

export default InputWrapper