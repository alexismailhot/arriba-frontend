interface Props {
    type: 'earth' | 'confounded' | 'thinking-face' | 'envelope-with-downwards-arrow-above'
}

const Emoji: React.FC<Props> = ({ type }) => {
    return (
        <span className={`emoji ${type}`} />
    )
}

export default Emoji