interface Props {
    color: 'white' | 'blue'
    className?: string
}

const LoadAnimation: React.FC<Props> = ({ color, className = "" }) => {
    return (
        <div className={`loader ${color} ${className}`} />
    )
}

export default LoadAnimation