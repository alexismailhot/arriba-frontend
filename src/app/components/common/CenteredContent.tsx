const CenteredContent: React.FC = ({ children }) => {
    return (
        <div className='flex flex-1 flex-col items-center'>
            <div style={{ flex: 1 }} />
            {children}
            <div style={{ flex: 2 }} />
        </div>
    )
}

export default CenteredContent