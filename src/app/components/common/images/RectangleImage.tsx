interface Props {
    source: string
    alt: string
    width: string,
    className?: string
    onClick?: () => void
}

const RectangleImage: React.FC<Props> = ({ source, alt, width, className = '', onClick = () => {} }) => {
    return (
        <img
            src={source}
            alt={alt}
            style={{ width: width }}
            className={className}
            onClick={onClick}
        />
    )
}

export default RectangleImage