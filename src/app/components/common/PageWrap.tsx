const PageWrap: React.FC = ({ children }) => {
    return (
        <div className="flex flex-col flex-1 px-8 md:px-16">
            {children}
        </div>
    )
}

export default PageWrap