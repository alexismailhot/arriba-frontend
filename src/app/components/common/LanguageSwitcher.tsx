import useLanguageHelper from "app/hooks/language/useLanguageHelper"

const LanguageSwitcher = () => {
    const { getOppositeLanguage, changeLanguage } = useLanguageHelper()

    return (
        <div 
            className="cursor-pointer text-yellow-600 uppercase hover:text-yellow-700 font-normal"
            onClick={changeLanguage} 
        >
            {getOppositeLanguage()}
        </div>
    )
}

export default LanguageSwitcher