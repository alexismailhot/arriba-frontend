interface Props {
    text: string
    className?: string
    onClick: () => void
    disabled?: boolean
}

const CustomLink: React.FC<Props> = ({ text, className = "", onClick, disabled = false }) => {
    const handleClick = (): void => {
        if (!disabled) {
            onClick()
        }
    }

    return (
        <span 
            className={`link ${className} ${disabled ? "disabled" : ""}`} 
            onClick={handleClick}
        >
            {text}
        </span>
    )
}

export default CustomLink