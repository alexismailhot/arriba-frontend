interface Props {
    linkText: string
    email: string
    subject?: string
    body?: string
}

const MailToLink: React.FC<Props> = ({ linkText, email, subject = "", body = "" }) => {
    return (
        <a
            className="link"
            href={`mailto:${email}?subject=${subject}&body=${body}`}
        >
            {linkText}
        </a>
    )
}

export default MailToLink