import { useTranslation } from "react-i18next"
import Button from "app/components/common/Button"
import Modal from "app/components/common/modals/Modal"
import MailToLink from "app/components/common/links/MailToLink"

interface Props {
    title: string
    message: string
    buttonText: string
    onClose: () => void
    showContactUsText? : boolean
}

const ErrorModal: React.FC<Props> = ({ title, message, buttonText, onClose, showContactUsText = false }) => {
    const { t } = useTranslation()
    
    return (
        <Modal>
            <div>
                <h2>
                    {title}
                </h2>
                
                <p className="mb-5">
                    {message}
                </p>

                {showContactUsText && (
                    <p className="mb-5">
                        {t("if_the_problem_persists_you_can")}
                        <MailToLink 
                            linkText={t("contact_us_here")} 
                            email={process.env.REACT_APP_SUPPORT_EMAIL!} 
                        />
                    </p>
                )}

                <Button
                    content={buttonText}
                    purpose='primary'
                    onClick={onClose}
                />
            </div>
        </Modal>
    )
}

export default ErrorModal