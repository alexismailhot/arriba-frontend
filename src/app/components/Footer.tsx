import { useTranslation } from "react-i18next"
import useExternalRedirection from "app/hooks/redirections/useExternalRedirection"
import RectangleImage from "app/components//common/images/RectangleImage"
import LanguageSwitcher from "app/components/common/LanguageSwitcher"
import InstagramLogo from "app/assets/images/png/instagram_logo_white.png"
import FacebookLogo from "app/assets/images/png/facebook_logo_white.png"
import TwitterLogo from "app/assets/images/png/twitter_logo_white.png"

const Footer = () => {
    const { t } = useTranslation()
    const { 
        redirectToTermsAndConditions, 
        redirectToPrivacyPolicy,
        redirectToInstagramProfile,
        redirectToFacebookProfile,
        redirectToTwitterProfile
    } = useExternalRedirection()
    
    return (
        <div className="bg-blue-normal px-10 py-4">
            <div className="flex flex-col justify-between items-center md:flex-row">
                <div className="flex flex-col items-center md:items-start mb-8 md:mb-0">
                    <p 
                        className="text-white text-sm cursor-pointer" 
                        onClick={redirectToTermsAndConditions}
                    >
                        {t('terms_and_conditions')}
                    </p>
                    <p 
                        className="text-white text-sm cursor-pointer"
                        onClick={redirectToPrivacyPolicy}
                    >
                        {t('privacy_policy')}
                    </p>
                    <LanguageSwitcher />
                </div>

                <div className="flex flex-col items-center">
                    <p className="text-white uppercase mb-2">{t('follow_us')}</p>
                    <div className="flex">
                        <RectangleImage 
                            source={InstagramLogo} 
                            alt={t('follow_us_on_instagram')} 
                            className="cursor-pointer"
                            width={"32px"}    
                            onClick={redirectToInstagramProfile}                    
                        />
                        <RectangleImage 
                            source={FacebookLogo} 
                            alt={t('follow_us_on_facebook')} 
                            width={"32px"}     
                            className="ml-3 cursor-pointer"  
                            onClick={redirectToFacebookProfile}                      
                        />
                        <RectangleImage 
                            source={TwitterLogo} 
                            alt={t('follow_us_on_twitter')} 
                            width={"32px"}      
                            className="ml-3 cursor-pointer" 
                            onClick={redirectToTwitterProfile}                 
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer