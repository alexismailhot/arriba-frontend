import { useTranslation } from "react-i18next"
import useExternalRedirection from "app/hooks/redirections/useExternalRedirection"
import RectangleImage from "app/components/common/images/RectangleImage"
import ArribaRainbowLogo from "app/assets/images/png/arriba_rainbow_logo.png"
import Button from "app/components/common/Button"
import useRedirection from "app/hooks/redirections/useRedirection"
import LanguageSwitcher from "app/components/common/LanguageSwitcher"

export interface MobileMenuModalProps {
    isAuthenticated: boolean
    closeMobileMenu: () => void
}

const MobileMenuModal: React.FC<MobileMenuModalProps> = ({ isAuthenticated, closeMobileMenu }) => {
    const { t } = useTranslation()
    const { redirectToHome, redirectToFAQ, redirectToAboutUs } = useExternalRedirection()
    const { redirectToLogin, redirectToDashboard, redirectToSubscribe } = useRedirection()

    const goToSubscribe = (): void => {
        closeMobileMenu()
        redirectToSubscribe()
    }

    const goToDashboard = (): void => {
        closeMobileMenu()
        redirectToDashboard()
    }

    const goToLogin = (): void => {
        closeMobileMenu()
        redirectToLogin()
    }

    return (
        <div className="mobile-menu-modal">
            <div className="mobile-menu-modal-content">
                <span 
                    className="cursor-pointer text-3xl text-white" 
                    onClick={closeMobileMenu}
                >
                    &times;
                </span>

                <div className="flex flex-col items-center mt-6">
                    <RectangleImage 
                        className="cursor-pointer mb-6"
                        onClick={redirectToHome}
                        source={ArribaRainbowLogo} 
                        alt="Back to home" 
                        width="75px" 
                    />

                    <Button 
                        className="mb-6 white" 
                        content={t('subscribe')} 
                        purpose={"secondary"}
                        onClick={goToSubscribe}
                    />

                    <div 
                        className="cursor-pointer uppercase text-white mb-6" 
                        onClick={redirectToFAQ}
                    >
                        Faq
                    </div>

                    <div 
                        className="cursor-pointer uppercase text-white mb-6"
                        onClick={redirectToAboutUs}
                    >
                        {t('about_us')}
                    </div>

                    {isAuthenticated ? (
                        <div 
                            className="cursor-pointer uppercase text-white mb-6" 
                            onClick={goToDashboard}
                        >
                            {t('my_account')}
                        </div>
                    ) : (
                        <div 
                            className="cursor-pointer uppercase text-white mb-6" 
                            onClick={goToLogin}
                        >
                            {t('login')}
                        </div>
                    )}

                    <LanguageSwitcher />
                </div>
            </div>
        </div>
    )
}

export default MobileMenuModal
