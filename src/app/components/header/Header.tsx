import { useTranslation } from "react-i18next"
import { useEffect, useState } from "react"
import { ReactComponent as MobileMenuBurgerIcon } from 'app/assets/images/svg/mobile_menu_burger.svg'
import ArribaLogo from "app/assets/images/png/arriba_logo_black.png"
import RectangleImage from "app/components/common/images/RectangleImage"
import useTokenCookie from "app/hooks/cookies/useTokenCookie"
import Button from "app/components/common/Button"
import LanguageSwitcher from "app/components/common/LanguageSwitcher"
import useRedirection from "app/hooks/redirections/useRedirection"
import useExternalRedirection from "app/hooks/redirections/useExternalRedirection"
import MobileMenuModal from "app/components/header/MobileMenuModal"

const Header = () => {
    const { t } = useTranslation()
    const { token } = useTokenCookie()
    const { redirectToLogin, redirectToDashboard, redirectToSubscribe } = useRedirection()
    const { redirectToHome, redirectToFAQ, redirectToAboutUs } = useExternalRedirection()

    const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false)
    const [isMobileMenuOpen, setIsMobileMenuOpen] = useState<boolean>(false)

    useEffect(() => {
        if (token !== undefined) {
            setIsAuthenticated(true)
        } else {
            setIsAuthenticated(false)
        }
    }, [token])

    const openMobileMenu = (): void => {
        setIsMobileMenuOpen(true)
    }

    const closeMobileMenu = (): void => {
        setIsMobileMenuOpen(false)
    }

    return (
        <div className="flex items-center sticky top-0 w-full h-16 md:h-24 bg-white shadow-md">
            {isMobileMenuOpen && (
                <MobileMenuModal 
                    isAuthenticated={isAuthenticated}
                    closeMobileMenu={closeMobileMenu} 
                />
            )}
            
            <div className="flex items-center justify-between w-full px-6 lg:px-16"> 
                <div className="flex items-center">
                    <div 
                        className="flex h-16 md:hidden" 
                        onClick={openMobileMenu}
                    >
                        <MobileMenuBurgerIcon 
                            title="Open menu" 
                            className="cursor-pointer w-5 mr-3" 
                        />
                    </div>

                    <RectangleImage 
                        className="cursor-pointer hidden md:block"
                        onClick={redirectToHome}
                        source={ArribaLogo} 
                        alt="Back to home" 
                        width="80px" 
                    />
                    <RectangleImage 
                        className="cursor-pointer md:hidden"
                        onClick={redirectToHome}
                        source={ArribaLogo} 
                        alt="Back to home" 
                        width="66px" 
                    />
                </div>
                
                <div className="hidden md:flex">
                    <div 
                        className="cursor-pointer uppercase mr-4 lg:mr-6 text-sm" 
                        onClick={redirectToFAQ}
                    >
                        Faq
                    </div>

                    <div 
                        className="cursor-pointer uppercase mr-4 lg:mr-6 text-sm"
                        onClick={redirectToAboutUs}
                    >
                        {t('about_us')}
                    </div>

                    {isAuthenticated ? (
                        <div 
                            className="cursor-pointer uppercase text-sm" 
                            onClick={redirectToDashboard}
                        >
                            {t('my_account')}
                        </div>
                    ) : (
                        <div 
                            className="cursor-pointer uppercase text-sm" 
                            onClick={redirectToLogin}
                        >
                            {t('login')}
                        </div>
                    )}
                </div>

                <div className="flex items-center">
                    <Button 
                        className="mr-2"
                        content={t('subscribe')}
                        purpose='secondary'
                        onClick={redirectToSubscribe}
                    />

                    <div className="hidden md:block">
                        <LanguageSwitcher />
                    </div>
                </div>
                
            </div>
        </div>
    )
}

export default Header