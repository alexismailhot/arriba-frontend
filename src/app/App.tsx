import Header from "app/components/header/Header"
import Footer from "app/components/Footer"
import AppContent from "app/routes/AppContent"

const App = () => {
    return (
        <div className="flex flex-col min-h-full">
            <Header />

            <div className="flex flex-col flex-1">
                <AppContent />
                
                <Footer />
            </div>
        </div>
    )
}

export default App